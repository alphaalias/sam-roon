module.exports = {
  siteMetadata: {
    title: `Alpha Alias - The Project First Agency`,
    siteUrl: `https://www.alphaalias.com/`,
    description: `An international digital agency focused on providing greater value to web-based products by optimizing operational leadership.`,
    author: 'arte'
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: "gatsby-source-wordpress",
      options: {
        baseUrl: "94-237-84-52.de-fra1.upcloud.host/wordpress/",
        protocol: "https",
        hostingWPCOM: false,
        useACF: true,
        verboseOutput: true
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-plugin-sass`,
    // 'gatsby-disable-404',
    `gatsby-plugin-force-trailing-slashes`,
    `gatsby-plugin-transition-link`,
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `alpha-alias`,
        short_name: `alpha`,
        start_url: `/`,
        background_color: `#6715d1`,
        theme_color: `#6715d1`,
        display: `minimal-ui`,
        icon: `src/images/alphaalias-icon.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ]
}
