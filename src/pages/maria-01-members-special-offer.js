import React, { Component } from "react"
import { graphql } from 'gatsby'
import Link from 'gatsby-plugin-transition-link'
import Img from 'gatsby-image/withIEPolyfill'

import Layout from "../components/layout"
import SEO from "../components/seo"
import WhatWeDoContainerM from "../components/whatwedocontainerM"
import RunningText from "../components/runningText"
import { Parallax } from 'react-scroll-parallax'

import FooterContact from "../components/footerContact"
import Footer from "../components/footer"
import ScrollControl from "../components/scrollControl"
import { FacebookShareButton, TwitterShareButton, LinkedinShareButton} from 'react-share'

// const numbersString = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten'];

class PhilanthropyPage extends Component {

  constructor(props) {
    super(props)

    this.state = {
      activeApproach: 0
    }
  }

  // componentDidMount() {
  //   window.addEventListener('scroll', this.handleScroll);
  // }
  //
  // componentWillUnmount() {
  //
  //   window.removeEventListener('scroll', this.handleScroll);
  //
  // }
  //
  // handleScroll = () => {
  //
  //   if(window.innerWidth > 991) {
  //
  //     const opacityValue = Math.min(1, window.scrollY / 500);
  //
  //     this.folderContent.style.opacity = 1 - opacityValue;
  //     this.folderTalk.style.opacity = 1 - opacityValue;
  //     this.folderNew.style.opacity = 1 - opacityValue;
  //
  //   }
  //
  // }

  handleSwitchApproachDescription = (slideId) => {

    this.setState({
      activeApproach: slideId
    });

    // this.approachTitleSlider.slickGoTo(slideId);

  }

  handleSwitchOnclick = (slideId) => {

    this.approachCarousel.slickGoTo(slideId);

  }

  render() {

    // let approachCarouselSettings = {
    //   dots: false,
    //   arrows: false,
    //   infinite: false,
    //   speed: 500,
    //   slidesToShow: 1,
    //   slidesToScroll: 1,
    //   variableWidth: true,
    //   cssEase: 'linear',
    //   swipeToSlide: true,
    //   afterChange: index => this.handleSwitchApproachDescription(index)
    // };

    // let approachTitleCarouselTitle = {
    //   dots: false,
    //   arrows: false,
    //   infinite: false,
    //   speed: 300,
    //   slidesToShow: 1,
    //   slidesToScroll: 1,
    //   cssEase: 'linear',
    //   touchMove: false,
    //   swipe: false,
    //   draggable: false,
    //   vertical: true
    // }

    // const { activeApproach } = this.state;
    const { wordpressPage, allWordpressAcfOptions } = this.props.data;
    // const lastCase = this.props.data.allWordpressWpCasesPosts.edges[0].node;
    // const lastNew = this.props.data.allWordpressWpNewsPosts.edges[0].node;
    const shareUrl = this.props.location.href;

      return(
        <Layout location={this.props.location}>
            
          <ScrollControl />
          <SEO title="Alpha Alias - The Project First Agency" description="An international digital agency focused on providing greater value to web-based products by optimizing operational leadership." />
          <section className="section folder __home-folder">
            <div className="triangle-item" id="folder-triangle">
              <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
                <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                    l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                    C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                    c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                    l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                    c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                    c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                    c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                    C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                    c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
              </svg>
            </div>
            <div className="container">
              <div className="row">
                <div className="col-md-9 col-xs-12">
                  <div className="folder-content-wrap" ref={folderContent => this.folderContent = folderContent}>
                    <Parallax y={[-20, 20]}>
                      <div className="folder-content">
                        <div className="subtitle white">
                          <p dangerouslySetInnerHTML={{ __html: wordpressPage.acf.slogan }} />
                        </div>
                        <div dangerouslySetInnerHTML={{ __html: wordpressPage.acf.main_title }} />
                            <div className="special-description">
                                <p dangerouslySetInnerHTML={{ __html: wordpressPage.acf.folder_description }} />
                            </div>
                            <div id="hero-buttons">
                            <Link
                                rel="canonical"
                                exit={{
                                    length: 0.3
                                }}
                                entry={{
                                    delay: 0.3
                                }}
                                to={ wordpressPage.acf.home_folder_button_link }
                                className="btn-link __border-theme __white-theme">
                                <span dangerouslySetInnerHTML={{ __html: wordpressPage.acf.folder_button_title }} className="btn-link-title" />
                        </Link>

                                </div>
                      </div>
                    </Parallax>
                  </div>
                </div>
                <div class="folder-share-wrap">
                    <div class="share-list">
                    <LinkedinShareButton url={shareUrl} />
                    <FacebookShareButton url={shareUrl} />
                    <TwitterShareButton url={shareUrl} />
                    </div>
                </div>
                <div className="folder-bg-wrap">
              <div className="triangle-wrap">
                <div className="triangle-item" id="folder-bg-triangle">
                  <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
                    <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                      l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                      C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                      c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                      l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                      c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                      c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                      c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                      C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                      c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
                  </svg>
                </div>
              </div>
            </div>
              </div>
            </div>
          </section>


          <section className="section about-review-section" id="white-switch-section">
          <div className="triangle-item" id="about-review-triangle">
            <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
              <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                    l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                    C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                    c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                    l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                    c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                    c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                    c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                    C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                    c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
            </svg>
          </div>
          <div className="show-on-scroll">
            <div className="about-review-preview">
              <Img className="about-review-preview-image" fluid={wordpressPage.acf.about_review_preview.localFile.childImageSharp.fluid} />
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-md-7 col-md-offset-1 col-xs-12">
                <Parallax y={[-20, 20]}>
                  <div className="show-on-scroll">
                    <div className="about-review-wrap">
                      <div className="subtitle violet">
                        <p dangerouslySetInnerHTML={{ __html: wordpressPage.acf.about_solution_review_slogan }} />
                      </div>
                      <div className="about-review-title" dangerouslySetInnerHTML={{ __html: wordpressPage.acf.about_review_title }} />
                      <div className="about-review-description">
                        <p dangerouslySetInnerHTML={{ __html: wordpressPage.acf.about_review_description }} />
                      </div>
                      <div className="about-review-author">
                        <div className="review-author-image">
                          <Img fluid={wordpressPage.acf.about_review_author_photo.localFile.childImageSharp.fluid} />
                        </div>
                        <div className="review-author-content">
                          <p className="review-author-title" dangerouslySetInnerHTML={{ __html: wordpressPage.acf.about_review_author }} />
                          <p className="review-author-position" dangerouslySetInnerHTML={{ __html: wordpressPage.acf.about_review_author_position }} />
                        </div>
                      </div>
                    </div>
                  </div>
                </Parallax>
              </div>
            </div>
          </div>
        </section>

          



          
    <section className="section article-challenge-section">
            <div className="show-on-scroll">
              <div className="job-challenge-preview">
              {wordpressPage.acf.article_challenge_preview.localFile
                  ?
                  <Img fluid={wordpressPage.acf.article_challenge_preview.localFile.childImageSharp.fluid}
                       className="job-challenge-preview-image"
                       objectPosition="50% 100%" />
                  : ''}
            </div>
        </div>
        <div className="container">
              <div className="row">
                <div className="col-md-11 col-md-offset-1 col-xs-12">
                  <Parallax y={[10, -10]}>
                    <div className="show-on-scroll">
                      <div className="job-challenge-block">
                        <div className="triangle-item" id="job-interview-triangle">
                          <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
                            <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                          l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                          C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                          c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                          l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                          c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                          c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                          c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                          C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                          c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
                          </svg>
                        </div>
                        <div className="row">
                          <div className="col-md-5 col-xs-12" id="what-well-do">
                            <div className="subtitle white">
                              <p dangerouslySetInnerHTML={{ __html: wordpressPage.acf.article_challenge_subtitle }} />
                            </div>
                            <div className="job-challenge-block-title" dangerouslySetInnerHTML={{ __html: wordpressPage.acf.article_challenge_title }} />
                          </div>
                          <div className="col-md-7 col-xs-12">
                            <div className="job-challenge-block-content" dangerouslySetInnerHTML={{ __html: wordpressPage.acf.article_challenge_content }} />
                          </div>
                        </div>
                      </div>
                    </div>
                  </Parallax>
                </div>
              </div>
            </div>

    </section>
          <WhatWeDoContainerM />
          <RunningText data={ allWordpressAcfOptions } />
          <FooterContact />
          {/* <Footer /> */}
        </Layout>
      )
  }

}

export default PhilanthropyPage

export const pageQuery = graphql`
    {
        wordpressPage (id: { eq: "bd53a969-63c5-5ffc-a34b-e0bded3b4dfd" }) {
            id,
            title,
            acf{
                main_title,
                slogan,
                folder_description,
                folder_button_title,
                home_folder_button_link,
                about_solution_review_slogan,
                about_review_title,
                about_review_description,
                about_review_author,
                about_review_author_position,
                about_review_author_photo{
                    localFile{
                        childImageSharp{
                            fluid (maxWidth: 100){
                                aspectRatio,
                                sizes,
                                src,
                                srcSet
                            }
                        }
                    }
                }
                about_review_preview{
                    localFile{
                        childImageSharp{
                            fluid (maxWidth: 2000) {
                                aspectRatio,
                                sizes,
                                src,
                                srcSet
                            }
                        }
                    }
                }
                about_review_title_2,
                about_review_description_2,
                cta_section_title,
                cta_section_text,
                cta_section_button_title,
                cta_section_button_link,
                article_challenge_subtitle,
                article_challenge_title,
                article_challenge_content, 
                terms_and_conditions_title,
                terms_and_conditions_text,
                article_challenge_preview{
                    localFile{
                        childImageSharp{
                            fluid (maxWidth: 2000){
                                aspectRatio,
                                src,
                                sizes,
                                srcSet
                            }
                        }
                    }
                }
                
            }
            featured_media{
                localFile{
                    childImageSharp{
                        fluid (maxWidth: 2000){
                            aspectRatio,
                            sizes,
                            src,
                            srcSet
                        }
                    }
                }
            }
        }
        
        
        allWordpressWpNewsPosts (limit: 1){
            edges{
                node{
                    slug,
                    title,
                    categories{
                        id,
                        name
                    }
                }
            }
        }
        allWordpressAcfOptions{
            edges{
                node{
                    options{
                        running_text_title
                    }
                }
            }
        }
    }
`
