import React, { Component } from "react"
import { graphql } from 'gatsby'
import Img from 'gatsby-image'
import { Parallax } from 'react-scroll-parallax'
import Layout from "../components/layout"
import ScrollControl from "../components/scrollControl"
import SEO from "../components/seo"
import Folder from "../components/folder"
import FooterContact from "../components/footerContact"
import Footer from "../components/footer"
import Map from "../components/map"

class About extends Component{

  render() {

    const { wordpressPage, allWordpressAcfOptions } = this.props.data;

    return(
      <Layout location={this.props.location}>
        <ScrollControl />
        <SEO title="Alpha Alias - The Project First Agency | About" description="An international digital agency focused on providing greater value to web-based products by optimizing operational leadership." />
        <Folder data={wordpressPage}/>
        <section className="section about-review-section" id="white-switch-section">
          <div className="triangle-item" id="about-review-triangle">
            <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
              <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                    l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                    C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                    c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                    l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                    c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                    c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                    c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                    C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                    c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
            </svg>
          </div>
          <div className="show-on-scroll">
            <div className="about-review-preview">
              <Img className="about-review-preview-image" fluid={wordpressPage.acf.about_review_preview.localFile.childImageSharp.fluid} />
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-md-7 col-md-offset-1 col-xs-12">
                <Parallax y={[-20, 20]}>
                  <div className="show-on-scroll">
                    <div className="about-review-wrap">
                      <div className="subtitle violet">
                        <p dangerouslySetInnerHTML={{ __html: wordpressPage.acf.about_solution_review_slogan }} />
                      </div>
                      <div className="about-review-title" dangerouslySetInnerHTML={{ __html: wordpressPage.acf.about_review_title }} />
                      <div className="about-review-description">
                        <p dangerouslySetInnerHTML={{ __html: wordpressPage.acf.about_review_description }} />
                      </div>
                      <div className="about-review-author">
                        <div className="review-author-image">
                          <Img fluid={wordpressPage.acf.about_review_author_photo.localFile.childImageSharp.fluid} />
                        </div>
                        <div className="review-author-content">
                          <p className="review-author-title" dangerouslySetInnerHTML={{ __html: wordpressPage.acf.about_review_author }} />
                          <p className="review-author-position" dangerouslySetInnerHTML={{ __html: wordpressPage.acf.about_review_author_position }} />
                        </div>
                      </div>
                    </div>
                  </div>
                </Parallax>
              </div>
            </div>
          </div>
        </section>
        <section className="section about-values-section">
          <Parallax y={[10, -20]}>
            <div className="show-on-scroll">
              <div className="about-values-preview">
                <Img className="about-values-preview-image" fluid={wordpressPage.acf.about_solution_preview.localFile.childImageSharp.fluid} />
              </div>
            </div>
          </Parallax>
          <div className="container">
            <div className="show-on-scroll">
              <div className="about-values-wrap">
                <div className="subtitle violet">
                  <p dangerouslySetInnerHTML={{ __html: wordpressPage.acf.about_solution_values_slogan }} />
                </div>
                <div className="about-values-title" dangerouslySetInnerHTML={{ __html: wordpressPage.acf.about_values_title }} />
                <div className="about-values-list">
                  { wordpressPage.acf.about_values_list.map((value, key) => (
                    <div className="about-values-list-item" key={key}>
                      <div className="about-value-icon">
                        {value.about_value_icon.localFile
                          ?
                          <img src={value.about_value_icon.localFile.publicURL} alt={value.about_value_title}/>
                          : ''}
                      </div>
                      <h5 className="title-5 about-value-title violet" dangerouslySetInnerHTML={{ __html: value.about_value_title }} />
                      <div className="about-value-description">
                        <p dangerouslySetInnerHTML={{ __html: value.about_value_description }} />
                      </div>
                    </div>
                  )) }
                </div>
              </div>
            </div>
          </div>
        </section>
        <Map data={allWordpressAcfOptions} />
        {/* <FooterContact /> */}
        <Footer data={wordpressPage} />
      </Layout>
    )

  }

}

export default About

export const pageQuery = graphql`
    {
        wordpressPage (id: { eq: "b60d5535-434a-54b8-99a1-e43f138101ff" }) {
            id,
            acf{
                main_title,
                slogan,
                folder_description,
                about_solution_review_slogan,
                about_review_title,
                about_review_description,
                about_review_author,
                about_review_author_position,
                about_review_author_photo{
                    localFile{
                        childImageSharp{
                            fluid (maxWidth: 100){
                                aspectRatio,
                                sizes,
                                src,
                                srcSet
                            }
                        }
                    }
                }
                about_solution_values_slogan,
                about_values_title,
                about_values_list{
                    about_value_title,
                    about_value_description,
                    about_value_icon{
                        localFile{
                            publicURL
                        }
                    }
                }
                about_review_preview{
                    localFile{
                        childImageSharp{
                            fluid (maxWidth: 2000) {
                                aspectRatio,
                                sizes,
                                src,
                                srcSet
                            }
                        }
                    }
                }
                about_solution_preview{
                    localFile{
                        childImageSharp{
                            fluid (maxWidth: 2000) {
                                aspectRatio,
                                sizes,
                                src,
                                srcSet
                            }
                        }
                    }
                }
                next_step
                  next_step_links {
                step_name
                step_link
          }
            }
            featured_media{
                localFile{
                    childImageSharp{
                        fluid (maxWidth: 2000){
                            aspectRatio,
                            sizes,
                            src,
                            srcSet
                        }
                    }
                }
            }
        }
        allWordpressAcfOptions{
            edges{
                node{
                    options{
                        contacts_title,
#                        contacts_label,
#                        address,
                        offices_list {
                            office_title,
                            office_location,
                            office_address,
                        }
                    }
                }
            }
        }
    }
`