import React, { Component } from 'react';
import { graphql } from 'gatsby'
import Img from 'gatsby-image'
import Slider from 'react-input-slider'

import Layout from "../components/layout"
import SEO from "../components/seo"
import Footer from "../components/footer"
import ScrollControl from "../components/scrollControl"
import Folder from "../components/folder"
// import FeedbackForm from "../components/feedbackForm"
import Map from "../components/map"
import HubspotForm from 'react-hubspot-form'

let moveContainer;
let moveContentContainer;
let moveList;
let moveWrap;

class Contact extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isMoving: false,
      moveValue: 0,
      moveEndValue: 0,
      moveMaxValue: 0,
      activeSlideId: 0,
      snapPoints: [0],
      scrollValue: 0,
      scrollMaxValue: 0
    }
  }
  componentDidMount(){
    //$('iframe').contents().find('body').css('backgroundColor', 'white');
    window.onload = function() {
      let myiFrame = document.getElementById("hs-form-iframe-0");
      let doc = myiFrame.contentDocument.getElementsByTagName('form')[0] ;
      let TxtArea = doc.getElementsByTagName('textarea')[0];
      //console.log('Aaya',TxtArea);
      TxtArea.style.backgroundColor='white';
      
   }
    
  }

  // componentDidMount() {

  //   let { snapPoints } = this.state;

  //   moveContainer = document.getElementById('move-wrap-container');
  //   moveContentContainer = document.getElementById('move-wrap-container-content');
  //   moveWrap = document.getElementById('move-wrap');

  //   moveList = moveContainer.childNodes

  //   let point = 0;

  //   moveList.forEach(function(currentValue) {

  //     point += currentValue.getBoundingClientRect().width;

  //     snapPoints.push(point);

  //   });

  //   this.setSliderLimit(snapPoints[snapPoints.length - 2], moveContainer.getBoundingClientRect().width);

  // }

  // handleStartMove = (event) => {

  //   this.setState({
  //     isMoving: true
  //   });

  //   if (event.clientX) {

  //     this.setState({
  //       moveEndValue: event.clientX
  //     })

  //   }

  // }

  // handleStopMove = () => {

  //   this.setState({
  //     isMoving: false
  //   });

  // }

  // handleIsMoving = (event) => {

  //   const { isMoving, moveEndValue, moveValue, snapPoints } = this.state;

  //   if(isMoving) {

  //     if(event.x && event.x >= 0) {

  //       this.setState({
  //         moveValue: event.x
  //       })

  //     } else {

  //       this.setState({
  //         moveValue: Math.max(0, Math.min(snapPoints[snapPoints.length - 2], (moveValue - ((event.clientX - moveEndValue) / 18))))
  //       })

  //     }

  //     moveContainer.style.transform = 'translateX(-'+ moveValue +'px)';
  //     moveContentContainer.style.transform = 'translateX(-'+ moveValue +'px)';

  //   }

  // }

  // setSliderLimit = (moveValue) => {
  //   this.setState({
  //     moveMaxValue: moveValue,
  //     scrollMaxValue: moveValue
  //   })
  // }

  // handleScrolling = () => {
  //   this.setState({
  //     scrollValue: moveWrap.scrollLeft
  //   })
  // }

  // handleSetScrollValue = (event) => {
  //   this.setState({
  //     scrollValue: event.x
  //   })
  //   moveWrap.scrollLeft = event.x;
  // }

  render() {

    const { wordpressPage, allWordpressAcfOptions } = this.props.data;

    const { isMoving, moveValue, moveMaxValue, scrollValue, scrollMaxValue } = this.state;

    return(
      <Layout location={this.props.location}>
        <ScrollControl />
        <SEO title="Alpha Alias - The Project First Agency | Contact" description="An international digital agency focused on providing greater value to web-based products by optimizing operational leadership." />
        <Folder data={wordpressPage} />
        <section className="section feedback-section" id="white-switch-section">
          <div className="triangle-item" id="feedback-triangle">
            <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
              <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                    l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                    C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                    c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                    l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                    c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                    c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                    c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                    C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                    c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
            </svg>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-md-5 col-md-offset-1 col-sm-6 col-xs-12">
                <div className="show-on-scroll">
                  <div className="feedback-title" dangerouslySetInnerHTML={{ __html: wordpressPage.acf.feedback_title }} />
                </div>
              </div>
              <div className="col-sm-6 col-xs-12">
                <div className="show-on-scroll">
                 {/* <FeedbackForm data={wordpressPage} /> */}
                 {/* <div className="cont-details"></div> */}
                 {/* <iframe class="contact-us-form" title="Contact us" src="https://share.hsforms.com/1T9nRjue9STOS9wUwjruDQA3cf19" width="100%" height="1071px" frameborder="0"></iframe> */}
                 <HubspotForm
                    portalId='5618205'
                    formId='4fd9d18e-e7bd-4933-92f7-05308ebb8340'
                    onSubmit={() => console.log('Submit!')}
                    onReady={(form) => console.log('Form ready!')}
                    loading={<div>Loading...</div>}
                   />
                </div>
              </div>
            </div>
          </div>
        </section>
        <Map data={allWordpressAcfOptions} />
        {/* <section className="section team-section">
          <div className="container">
            <div className="row">
              <div className="col-md-8 col-md-offset-2 col-xs-12">
                <div className="show-on-scroll">
                  <div className="team-section-title">
                    <div className="subtitle violet">
                      <p dangerouslySetInnerHTML={{ __html: wordpressPage.acf.team_subtitle }} />
                    </div>
                    <div className="team-title" dangerouslySetInnerHTML={{ __html: wordpressPage.acf.team_title }} />
                    <div className="team-description">
                      <p dangerouslySetInnerHTML={{ __html: wordpressPage.acf.team_description }} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="team-moving-wrap show-on-scroll">
            <div className="swipe-slider-wrap __team-theme">
              <p className="swipe-slider-wrap-title">Drag Me</p>
              <Slider
                axis="x"
                x={moveValue}
                xmax={moveMaxValue}
                onChange={this.handleIsMoving}
                onMouseDown={this.handleStartMove}
                onDragEnd={this.handleStopMove}
                className="swipe-slider horizontal"
              />
            </div>
            <div className="swipe-slider-wrap __team-theme __scroll-theme">
              <p className="swipe-slider-wrap-title">Drag Me</p>
              <Slider
                axis="x"
                x={scrollValue}
                xmax={scrollMaxValue}
                onChange={this.handleSetScrollValue}
                className="swipe-slider horizontal"
              />
            </div>
            <div
              className="team-wrap move-wrap"
              id="move-wrap"
              onMouseDown={this.handleStartMove}
              onMouseUp={this.handleStopMove}
              onMouseMove={this.handleIsMoving}
              onScroll={this.handleScrolling}
            >
              <div
                className={`team-wrap-container move-wrap-container ${ isMoving ? '__is-moving' : '' }`}>
                <div className="team-move-container-photo" id="move-wrap-container">
                  {
                    wordpressPage.acf.team_list.map((person, key) => (
                      <div className="team-item" key={`person-photo-${key}`}>
                        <div className="team-item-photo">
                          <Img fluid={person.photo.localFile.childImageSharp.fluid} className="team-item-photo-image" />
                        </div>
                      </div>
                    ))
                  }
                </div>
                <div className="team-move-container-content" id="move-wrap-container-content">
                  {
                    wordpressPage.acf.team_list.map((person, key) => (
                      <div className="team-item" key={`person-${key}`}>
                        <div className="team-item-content">
                          <h4 className="title-4">{ person.full_name }</h4>
                          <p className="team-item-position violet" dangerouslySetInnerHTML={{ __html: person.position }} />
                          <p className="team-item-contact-data">Tel. { person.phone }</p>
                          <p className="team-item-contact-data">Email: { person.email }</p>
                          <p className="team-item-contact-data">Languages: {
                            person.languages.map((language, key) => (<span key={`language-${key}`}>{language.language}<span className="coma">,</span> </span>))
                          }</p>
                        </div>
                      </div>
                    ))
                  }
                </div>
              </div>
            </div>
          </div>
        </section> */}
        <Footer data={wordpressPage}/>
      </Layout>
    )

  }

}

export default Contact;

export const pageQuery = graphql`
    {
        wordpressPage(id: {eq: "6c3bb94f-c4ce-5e4c-bf78-c05d852a8d78"}) {
            id,
            acf{
                main_title,
                slogan,
                folder_description,
                feedback_title,
                name_input_placeholder,
                e_mail_input_placeholder,
                phone_input_placeholder,
                message_input_placeholder,
                feedback_button_title,
                team_subtitle,
                team_title,
                team_description,
                team_list{
                    full_name,
                    position,
                    phone,
                    email,
                    languages{
                        language
                    }
                    photo{
                        localFile{
                            childImageSharp{
                                fluid{
                                    aspectRatio,
                                    src,
                                    sizes,
                                    srcSet
                                }
                            }
                        }
                    }
                }
                next_step
                  next_step_links {
                step_name
                step_link
          }
            }
            featured_media{
                localFile{
                    childImageSharp{
                        fluid (maxWidth: 2000){
                            aspectRatio,
                            src,
                            sizes,
                            srcSet
                        }
                    }
                }
            }
        }
        allWordpressAcfOptions{
            edges{
                node{
                    options{
                        contacts_title,
#                        contacts_label,
#                        address,
                        offices_list {
                            office_title,
                            office_location,
                            office_address,
                        }
                    }
                }
            }
        }
    }
`