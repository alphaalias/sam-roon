import React, { Component } from 'react'
import { graphql } from 'gatsby'
import Layout from "../components/layout"
import SEO from "../components/seo"
import FooterContact from "../components/footerContact"
// import Footer from "../components/footer"
import ScrollControl from "../components/scrollControl"

class PostTemplate extends Component {

  render() {

    const currentPost = this.props.data.wordpressPost;

    return (

      <Layout>
        <ScrollControl />
        <SEO title={currentPost.title} />
        <div>
          <h1 dangerouslySetInnerHTML={{ __html: currentPost.title }}></h1>
          <p dangerouslySetInnerHTML={{ __html: currentPost.content }}></p>
        </div>
        <FooterContact />
        {/* <Footer /> */}
      </Layout>

    )

  }

}

export default PostTemplate;

export const pageQuery = graphql`
    query($id: String!){
        wordpressPost(id: { eq: $id }) {
            id,
            title
            content
        }
    }
`