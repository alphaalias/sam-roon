import React, { Component } from 'react'
import { graphql } from 'gatsby'
import Layout from "../components/layout"
import SEO from "../components/seo"
import ScrollControl from "../components/scrollControl"
import Header from "../components/header"
import FooterContact from "../components/footerContact"
import Footer from "../components/footer"

class PageTemplate extends Component {

  render() {

    const currentPage = this.props.data.wordpressPage;

    return (

      <Layout>
        <ScrollControl />
        <SEO title={`Alpha Alias - The Project First Agency | ${ currentPage.title }`} description="An international digital agency focused on providing greater value to web-based products by optimizing operational leadership." />
        <Header siteTitle={currentPage.title} violetTheme="true" location={this.props.location} />
        <section className="section article-section default-page-section" id="white-switch-section">
            <div className="container">
              <div className="row">
                <div className="col-xs-12">
                  <h1 dangerouslySetInnerHTML={{ __html: currentPage.title }} />
                  <div dangerouslySetInnerHTML={{ __html: currentPage.content }} />
                </div>
              </div>
            </div>
        </section>
        <FooterContact />
        {/* <Footer /> */}
      </Layout>

    )

  }

}

export default PageTemplate;

export const pageQuery = graphql`
  query($id: String!) {
      wordpressPage(id: { eq: $id }) {
          title
          content
      }
  }
`