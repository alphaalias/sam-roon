import React, { Component } from 'react'
import { graphql } from 'gatsby'
import { Parallax } from 'react-scroll-parallax'
import Layout from "../components/layout"
import SEO from "../components/seo"
import Folder from "../components/folder"
import Img from 'gatsby-image'
import Slider from 'react-slick'
import FooterContact from "../components/footerContact"
import Footer from "../components/footer"
import ScrollControl from "../components/scrollControl"
// import CountUp from 'react-countup';


class CaseTemplate extends Component {

  componentDidMount() {

    window.addEventListener('scroll', this.handleCaseScroll);

  }

  componentWillUnmount() {

    window.removeEventListener('scroll', this.handleCaseScroll);

  }

  

  onTestimonialPrev = () => {

    this.slider.slickPrev();

  }

  onTestimonialNext = () => {

    this.slider.slickNext();

  }

  render() {

    const currentCase = this.props.data.wordpressWpCasesPosts;
    
    const testimonialCarouselSettings = {
      dots: false,
      arrows: false,
      infinite: false,
      speed: 800,
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: false,
      cssEase: 'cubic-bezier(.7,.5,.6,1)',
      swipeToSlide: true
    }

    return (

      <Layout location={this.props.location} >
        <ScrollControl />
        <SEO title={`Alpha Alias - The Project First Agency | ${currentCase.title}`} description="An international digital agency focused on providing greater value to web-based products by optimizing operational leadership." />
        <div className="case-folder-wrap">
          <Folder data={currentCase} />
        </div>
        <section className="section case-detail-section" id="case-template-wrap">
          <div className="container">
            <div className="row">
              <div className="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <div className="case-detail-wrap">
                  <div className="triangle-wrap">
                    <div className="triangle-item" id="detail-triangle">
                      <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
                        <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                    l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                    C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                    c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                    l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                    c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                    c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                    c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                    C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                    c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
                      </svg>
                    </div>
                  </div>
                  {/* <div className="subtitle white">
                    <p dangerouslySetInnerHTML={{ __html: currentCase.acf.slogan }} />
                  </div> */}
                  <h1 className="title-3 white" dangerouslySetInnerHTML={{ __html: currentCase.title }} id="case-h1"/>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="section testimonial-section" id="white-switch-section">
          <div className="container">
            <div className="row">
              <div className="col-xs-12 col-md-8">
                <div className="show-on-scroll">
                  <div className="testimonial-wrap">
                    <p className="subtitle testimonial-wrap-title violet" dangerouslySetInnerHTML={{ __html: currentCase.acf.testimonials_subtitle }} />
                    <Slider {...testimonialCarouselSettings}  className="testimonial-wrap-list" ref={slider => (this.slider = slider)}>
                      {
                        currentCase.acf.testimonial_list.map((testimonial, key) => (
                          <div className="testimonial-item" key={key}>
                            <p className="" dangerouslySetInnerHTML={{ __html: testimonial.title }} />
                            <div className="testimonial-item-detail">
                              {
                                testimonial.author_logo
                                  ?
                                  <div className="author-logo">
                                    <Img fluid={testimonial.author_logo.localFile.childImageSharp.fluid} className="author-logo-image" />
                                  </div>
                                  : ''
                              }
                              <p className="author-title" dangerouslySetInnerHTML={{ __html: testimonial.author }} />
                              {/* <p className="author-position violet" dangerouslySetInnerHTML={{ __html: testimonial.author_position }} /> */}
                            </div>
                          </div>
                        ))
                      }
                    </Slider>
                    {
                      currentCase.acf.testimonial_list.length > 1
                        ?
                        <div className="custom-carousel-nav-wrap">
                          <div className="custom-nav-item __prev" onClick={this.onTestimonialPrev}>
                            Last
                            <div className="custom-nav-item-arrow" />
                          </div>
                          <div className="custom-nav-item __next" onClick={this.onTestimonialNext}>
                            Next
                            <div className="custom-nav-item-arrow" />
                          </div>
                        </div>
                        : null
                    }
                  </div>
                </div>
              </div>
              <div className="col-xs-12 col-md-4">
                <div class="testmon-client white">
                  <div className="clientlist">
                    <p className="strong white">Client</p>
                    <div className="client-name" dangerouslySetInnerHTML={{ __html: currentCase.acf.client_name }} />
                  </div>
                  <div className="clientlist">
                    <p className="strong white">Services</p>
                    { currentCase.acf.service.map((items, key) => (
                    <div className="goal-list" key={key}>
                      
                      <span className="about-value-title" dangerouslySetInnerHTML={{ __html: items.services_item }} />
                     
                    </div>
                  )) }
                  </div>
                  <div className="clientlist">
                    <p className="strong white">Industry</p>
                    <div className="client-name" dangerouslySetInnerHTML={{ __html: currentCase.acf.industry_name }} />
                  </div>
                
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="section challenge-section">
          <div className="container">
            <div className="show-on-scroll">
              <div className="challenge-preview">
                <Img fluid={currentCase.acf.challenge_block_preview.localFile.childImageSharp.fluid} className="challemge-preview-image" />
              </div>
            </div>
            <div className="row">
              <div className="col-xs-12">
                <Parallax y={[-10, 10]}>
                  <div className="show-on-scroll">
                    <div className="challenge-wrap">
                      {/*<div className="triangle-item" id="challenge-triangle">*/}
                        {/*<svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">*/}
                          {/*<path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2*/}
                        {/*l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9*/}
                        {/*C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8*/}
                        {/*c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4*/}
                        {/*l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8*/}
                        {/*c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2*/}
                        {/*c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9*/}
                        {/*c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5*/}
                        {/*C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7*/}
                        {/*c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>*/}
                        {/*</svg>*/}
                      {/*</div>*/}
                      <p className="subtitle" dangerouslySetInnerHTML={{ __html: currentCase.acf.the_challenge_case_subtitle }} />
                      <div className="challenge-wrap-title" dangerouslySetInnerHTML={{ __html: currentCase.acf.challenge_block_title }} />
                    </div>
                  </div>
                </Parallax>
              </div>
            </div>
          </div>
        </section>
        <section className="goal-section" >
          <div className="container">
          <div className="goal-content">
            <div className="goal-list">
              <div className></div>
                <h5 dangerouslySetInnerHTML={{ __html: currentCase.acf.goal_title }} />
                <ul>
                   { currentCase.acf.goal_lists.map((goal, key) => (
                   
                      
                      <li className="about-value-title" dangerouslySetInnerHTML={{ __html: goal.goal_list_item }} />
                      
                    
                  )) }</ul>
            </div>
          </div>
          </div>
        </section>
        <section className="section solution-section">
          <div className="start-fixed" ref={dotStartFixed => this.dotStartFixed = dotStartFixed} />
          <div className="container">
            <div className="row">
            <div className="col-md-12 col-sm-12 col-xs-12">
                <div className="solution-title-wrap" ref={fixedWrap => this.fixedWrap = fixedWrap}>
                  <p className="subtitle violet" dangerouslySetInnerHTML={{ __html: currentCase.acf.the_solution_case_subtitle }} />
                  <div className="solution-title">
                    <h4 className="title-4" dangerouslySetInnerHTML={{ __html: currentCase.acf.solution_block_title }} />
                  </div>
                </div>
              </div>
              </div>
              <div className="row">
                <div className="col-md-4 col-sm-6 col-xs-12">
                  <div className="solution-preview">
                    <Img fluid={currentCase.acf.solution_block_preview.localFile.childImageSharp.fluid} className="solution-preview-image" />
                  </div>
                </div>
                <div className="col-md-8 col-sm-6 col-xs-12" dangerouslySetInnerHTML={{ __html: currentCase.acf.short_description_right }}>
                
                </div>
              </div>
              <div className="row">
                <div className="col-md-8 col-sm-12 col-xs-12">
                <div className="solution-desc" dangerouslySetInnerHTML={{ __html: currentCase.acf.solution_description }}>
                    
                 </div>
               </div>
              </div>
              
          </div>
          
          <div className="end-fixed" ref={dotEndFixed => this.dotEndFixed = dotEndFixed} />
        </section>
        <section class="the-results">
        <div className="container">
        <div className="row">
              <div className="col-md-12 col-sm-12 col-xs-12">
              <div className="solution-title-wrap" ref={fixedWrap => this.fixedWrap = fixedWrap}>
                  <p className="subtitle violet" dangerouslySetInnerHTML={{ __html: currentCase.acf.results_sub_title }} />
                  <div className="solution-title">
                    <h4 className="title-4" dangerouslySetInnerHTML={{ __html: currentCase.acf.results_title }} />
                  </div>
                </div>
                <div className="show-on-scroll">
                  <div className="solution-lists">
                  
                    {
                      currentCase.acf.result_data.map((result, key) => (
                        <div className="card" key={key}>
                          {/* <div className="solution-list-item-icons">
                            {
                                  <h3 className="counter-result violet" dangerouslySetInnerHTML={{ __html: result.result_number }} />
                             
                            }
                          </div> */}
                          <h5 className="title-5 solution-list-item-title violet" dangerouslySetInnerHTML={{ __html: result.result_title }} />
                          <div className="solution-list-item-description">
                            <p dangerouslySetInnerHTML={{ __html: result.result_description }} />
                          </div>
                        </div>
                      ))
                    }
                  </div>
                  <div className="result-desc">
                  <p className="result-data-bottom" dangerouslySetInnerHTML={{ __html: currentCase.acf.results_description_bottom }} />
                  </div>
                </div>
              </div>
            </div>
            </div>
        </section>
        <FooterContact />
        {/* <Footer /> */}
      </Layout>

    )

  }

}

export default CaseTemplate;

export const pageQuery = graphql`
    query($id: String!){
        wordpressWpCasesPosts(id: { eq: $id }) {
            id,
            title,
            featured_media{
                localFile{
                    childImageSharp{
                        fluid (quality:100, maxWidth: 2000){
                            aspectRatio,
                            sizes,
                            src,
                            srcSet
                        }
                    }
                }
            }
            acf{
              main_title,
              slogan,
              logo{
                  localFile{
                      childImageSharp{
                          fluid (quality:100, maxWidth: 122){
                              aspectRatio,
                              sizes,
                              src,
                              srcSet
                          }
                      }
                  }
              },
              client_name,
              industry_name,
              results_sub_title,
              results_title,
              result_data {
                result_title
                result_description
                result_sign
              }
              results_description_bottom,
              service {
                services_item
              },
              testimonials_subtitle,
              testimonial_list{
                  title,
                  author,
                  author_position,
                  author_logo{
                      localFile{
                          childImageSharp{
                              fluid (quality:100, maxWidth: 52){
                                  aspectRatio,
                                  sizes,
                                  src,
                                  srcSet
                              }
                          }
                      }
                  }
              },
              the_challenge_case_subtitle,
              challenge_block_title,
              challenge_block_preview{
                  localFile{
                      childImageSharp{
                          fluid(quality:100){
                              aspectRatio,
                              sizes,
                              src,
                              srcSet
                          }
                      }
                  }
              },
              the_solution_case_subtitle,
              solution_block_title,
              short_description_right,
              solution_description,
              solution_block_preview {
                  localFile{
                      childImageSharp{
                          fluid(quality:100){
                              aspectRatio,
                              sizes,
                              src,
                              srcSet
                          }
                      }
                  }
              },
              solution_list{
                  title,
                  description,
                  icon{
                      localFile{
                          publicURL
                      }
                  }
              },
              goal_title,
              goal_lists {
                goal_list_item
              }
          }
        }
    }
`