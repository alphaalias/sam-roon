import React, { Component } from 'react'
import { graphql } from "gatsby"
import Header from "../components/header"
import ScrollControl from "../components/scrollControl"
import SEO from "../components/seo"
import Img from 'gatsby-image'
import { FacebookShareButton, TwitterShareButton, LinkedinShareButton } from 'react-share'
import { ParallaxProvider } from 'react-scroll-parallax'
import Parser from 'html-react-parser'
import Footer from "../components/footer"
import "../styles/pillar.scss"

let pillercontent;

class Pillar extends Component {

  constructor(props) {

    super(props)
    this.state = {
      startScrolling: false,
      y: 0,
      maxY: 0,
      enableMoveScrollControl: true,
      isActive: "",
    }

  }

  componentDidMount() {

    setTimeout(this.handleScroll_1, 1000);
    window.addEventListener('scroll', this.handleScroll_1);
    window.addEventListener('scroll', this.listenScrollEvent);
    window.addEventListener('scroll', this.handleCaseScroll);
    pillercontent = document.getElementById('piller-content');
  }

  componentWillUnmount() {

    window.removeEventListener('scroll', this.handleScroll_1);

  }
  handleScroll_1 = () => {

    if (window.scrollY > 0) {

      this.setState({
        startScrolling: true,
        theposition: window.pageYOffset
      })

    } else {

      this.setState({
        startScrolling: false,
        theposition: window.pageYOffset
      })

    }

  }

  handleCaseScroll = () => {

    const dotStartPosition = this.dotStartFixed.getBoundingClientRect().top;
    const dotEndPosition = this.dotEndFixed.getBoundingClientRect().top - window.innerHeight;

    if (dotStartPosition < 0 && window.innerWidth > 991 && pillercontent.clientHeight > window.innerHeight) {

      this.fixedWrap.style.position = 'fixed';
      this.fixedWrap.style.top = 0;
      this.fixedWrap.style.padding = '100px 0 0';

    } else {

      this.fixedWrap.style.position = 'relative';
      this.fixedWrap.style.padding = '0';

    }

    if (dotEndPosition < 0 && window.innerWidth > 991 && pillercontent.clientHeight > window.innerHeight) {

      this.fixedWrap.style.transform = 'translate(0px,' + dotEndPosition + 'px)'

    } else {

      this.fixedWrap.style.transform = 'translate(0px,0px)'

    }

  }

  handleActiveClick = (key) => {
    this.setState({ isActive: key })
  }

  render() {

    const { wordpressWpPillar } = this.props.data
    const media = this.props.data.allWordpressWpMedia.edges;

    const shareUrl = this.props.location.href;

    const Content_Top = Parser(wordpressWpPillar.acf.pillar_starting_text, {

      replace: domNode => {

        if (domNode.name === 'img') {

          let image = media.filter(m => {

            return m.node.source_url === domNode.attribs.src;

          });

          if (!!image.length) {

            image = image[0].node.localFile;

            return <Img fluid={image.childImageSharp.fluid} />

          }

        }

      }

    });

    return (
      <main id="wrapper" className="menu-color">
        <Header siteTitle={wordpressWpPillar.title} violetTheme="true" location={this.props.location} />

        <ScrollControl />
        <SEO title={`Alpha Alias - The Project First Agency | ${(wordpressWpPillar.title).replace(/&#(\d{0,4});/g, function (fullStr, str) { return String.fromCharCode(str); })}`} description={wordpressWpPillar.acf.pillar_seo_description} />

        <ParallaxProvider>

          <section className="section folder __article-folder" id="white-switch-section">
            <div className="triangle-item" id="article-folder-triangle">
              <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
                <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                      l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                      C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                      c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                      l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                      c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                      c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                      c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                      C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                      c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
              </svg>
            </div>
            <div className="folder-bg-wrap">
              <div className="triangle-wrap">
                <div className="triangle-item" id="folder-bg-triangle">
                  <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
                    <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                      l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                      C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                      c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                      l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                      c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                      c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                      c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                      C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                      c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
                  </svg>
                </div>
              </div>
            </div>
            <div className="container">
              <div className="row">
                <div className="col-md-6 col-xs-12">
                  <div className="folder-content-wrap">
                    <div className="folder-content">
                      <h1 className="title-2 article-title" dangerouslySetInnerHTML={{ __html: wordpressWpPillar.title }} />
                    </div>
                  </div>
                </div>
                <div className="col-md-6 col-xs-12">
                  <div className="folder-main-preview">
                    {wordpressWpPillar.featured_media
                      ?
                      <Img fluid={wordpressWpPillar.featured_media.localFile.childImageSharp.fluid} objectPosition="50% 0" className="folder-image" />
                      : ''}
                  </div>
                </div>
              </div>
            </div>
            <div className="folder-share-wrap">
              <div className="share-list">
                <LinkedinShareButton url={shareUrl} />
                <FacebookShareButton url={shareUrl} />
                <TwitterShareButton url={shareUrl} />
              </div>
              <p className="article-date">Published on {wordpressWpPillar.date}</p>

            </div>
          </section>
          <section className="section article-section">
            <div className="triangle-item" id="article-triangle">
              <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
                <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                      l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                      C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                      c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                      l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                      c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                      c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                      c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                      C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                      c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
              </svg>
            </div>
            <div className="container">
              <div className="row">
                <div className="col-md-10 col-md-offset-1 col-xs-12">
                  <div className="show-on-scroll">
                    <div className="article-content __top-content top__list">
                      <p> {Content_Top}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="container">
              <div className="row">
                <div className="col-md-3 col-xs-12">
                  <div className="start-fixed" ref={dotStartFixed => this.dotStartFixed = dotStartFixed}></div>
                  <div id="piller_sidebar_table" ref={fixedWrap => this.fixedWrap = fixedWrap}>
                    <div className="__top-content pillar-table-content">
                      <h4>Table Of Contents</h4>

                      <ol>
                        {wordpressWpPillar.acf && wordpressWpPillar.acf.pillar_contents !== null && wordpressWpPillar.acf.pillar_contents.map((pillar_contents, key) => (

                          <li className="piller-siderbar" onClick={() => this.handleActiveClick(key)}>
                            <a href={"#piller" + key} className={this.state.isActive === key ? "active_table_title" : ''}>{Parser(pillar_contents.section_title)}</a>
                          </li>

                        ))}
                      </ol>
                      {/* {Content_Table} */}
                    </div>
                  </div>
                </div>
                <div className="col-md-9 col-xs-12">
                  <div className="show-on-scroll">
                    <div className="__top-content pillar-content" id="piller-content">
                      {
                        wordpressWpPillar.acf && wordpressWpPillar.acf.pillar_contents !== null && wordpressWpPillar.acf.pillar_contents.map((pillar_contents, key) => (

                          <div id={"piller" + key}>
                            {Parser(pillar_contents.section_title)}
                            {Parser(pillar_contents.section_content)}
                          </div>

                        ))
                      }
                      {/* {Content} */}
                    </div>
                  </div>
                  <div className="end-fixed" ref={dotEndFixed => this.dotEndFixed = dotEndFixed}></div>
                </div>
              </div>
            </div>

          </section>
        </ParallaxProvider>
        <Footer data={wordpressWpPillar}/>
      </main>
    )

  }

}

export default Pillar

export const pageQuery = graphql`
  query($id: String!){
    wordpressWpPillar(id: { eq: $id }) {
      id,
      slug,
      title,
      featured_media{
          localFile{
              childImageSharp{
                  fluid(quality:100){
                      aspectRatio,
                      src,
                      sizes,
                      srcSet
                  }
              }
          }
      }
          acf {
            pillar_section_content,
            pillar_starting_text,
            table_content,
            pillar_contents {
              section_title
              section_content
            }
            next_step
                  next_step_links {
                    step_name
                    step_link
                },
          }
          date(formatString: "DD/MM/YYYY")
    }
    allWordpressWpMedia{
      edges{
          node{
              source_url,
              localFile{
                  childImageSharp{
                      fluid(quality:100){
                          aspectRatio,
                          src,
                          sizes,
                          srcSet
                      }
                  }
              }
          }
      }
  }
}`