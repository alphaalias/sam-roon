import React, { Component } from 'react'
import { graphql } from 'gatsby'
import { Parallax } from 'react-scroll-parallax'
import Img from 'gatsby-image/withIEPolyfill'
import Layout from "../components/layout"
import SEO from "../components/seo"
import ScrollControl from "../components/scrollControl"
import Folder from "../components/folder"
import RunningText from "../components/runningText"
import FooterContact from "../components/footerContact"
import Footer from "../components/footer"

class JobTemplate extends Component {

  render() {

    const currentJob = this.props.data.wordpressWpCareersPosts;
    const allWordpressAcfOptions = this.props.data.allWordpressAcfOptions;

    // console.log(currentJob);

    return(
      <Layout location={this.props.location}>
        <ScrollControl />
        <SEO title={`Alpha Alias - The Project First Agency | ${currentJob.title}`} description="An international digital agency focused on providing greater value to web-based products by optimizing operational leadership." />
        <div className="job-folder-wrap">
          <Folder data={currentJob} />
        </div>
        <section className="section about-solution-section" id="white-switch-section">
          <div className="triangle-item" id="about-solution-triangle">
            <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
              <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                    l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                    C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                    c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                    l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                    c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                    c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                    c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                    C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                    c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
            </svg>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-md-6 col-xs-12">
                <Parallax y={[-5, 10]}>
                  <div className="show-on-scroll">
                    <div className="solution-block">
                      <div className="subtitle violet">
                        <p dangerouslySetInnerHTML={{ __html: currentJob.acf.about_alpha_alias_subtitle }} />
                      </div>
                      <div className="solution-block-title" dangerouslySetInnerHTML={{ __html: currentJob.acf.about_solution_title }} />
                      <div className="solution-block-description">
                        <p dangerouslySetInnerHTML={{ __html: currentJob.acf.about_solution_description }} />
                      </div>
                    </div>
                  </div>
                </Parallax>
              </div>
              <div className="col-md-6 col-xs-12">
                <div className="show-on-scroll">
                  <div className="solution-block-preview">
                    <Img
                      fluid={currentJob.acf.about_solution_preview.localFile.childImageSharp.fluid}
                      className="solution-block-preview-image"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="section job-challenge-section">
          <Parallax y={[0, -10]}>
            <div className="show-on-scroll">
              <div className="job-challenge-preview">
                <Img
                  fluid={currentJob.acf.challenge_preview.localFile.childImageSharp.fluid}
                  className="job-challenge-preview-image"
                  objectPosition="50% 100%" />
              </div>
            </div>
          </Parallax>
          <div className="container">
            <div className="row">
              <div className="col-md-11 col-md-offset-1 col-xs-12">
                <div className="show-on-scroll">
                  <div className="job-challenge-block">
                    <div className="triangle-item" id="job-challenge-triangle">
                      <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
                        <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                      l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                      C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                      c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                      l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                      c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                      c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                      c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                      C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                      c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
                      </svg>
                    </div>
                    <div className="row">
                      <div className="col-md-4 col-xs-12">
                        <div className="subtitle white">
                          <p dangerouslySetInnerHTML={{ __html: currentJob.acf.about_the_job_subtitle }} />
                        </div>
                        <div className="job-challenge-block-title" dangerouslySetInnerHTML={{ __html: currentJob.acf.challenge_title }} />
                        <div className="job-challenge-block-description">
                          <p dangerouslySetInnerHTML={{ __html: currentJob.acf.challenge_description }} />
                        </div>
                      </div>
                      <div className="col-md-8 col-xs-12">
                        <div className="job-challenge-block-content" dangerouslySetInnerHTML={{ __html: currentJob.acf.challenge_content }} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="section job-solution">
          <div className="triangle-item" id="job-solution-triangle">
            <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
              <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                    l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                    C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                    c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                    l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                    c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                    c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                    c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                    C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                    c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
            </svg>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-md-6 col-xs-12">
                <Parallax y={[-10, 10]}>
                  <div className="show-on-scroll">
                    <div className="solution-block">
                      <div className="subtitle violet">
                        <p dangerouslySetInnerHTML={{ __html: currentJob.acf.area_of_operation_subtitle }} />
                      </div>
                      <div className="solution-block-title" dangerouslySetInnerHTML={{ __html: currentJob.acf.solution_title }} />
                      <div className="solution-block-description">
                        <p dangerouslySetInnerHTML={{ __html: currentJob.acf.solution_description }} />
                      </div>
                    </div>
                  </div>
                </Parallax>
              </div>
              <div className="col-md-6 col-xs-12">
                <div className="show-on-scroll">
                  <div className="solution-block-preview">
                     <Img fluid={currentJob.acf.solution_preview.localFile.childImageSharp.fluid} className="solution-block-preview-image" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <RunningText data={allWordpressAcfOptions} />
        <section className="section job-detail-section">
          <div className="container">
            <div className="job-detail-list">
              <div className="row">
                <div className="col-md-6 col-xs-12">
                  <Parallax y={[0, 20]}>
                    <div className="show-on-scroll">
                      <div className="job-detail-block violet">
                        <div className="triangle-item" id="responsibilities-triangle">
                          <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
                            <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                      l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                      C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                      c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                      l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                      c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                      c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                      c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                      C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                      c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
                          </svg>
                        </div>
                        <div className="job-detail-block-title">
                          <h3 className="title-3">Responsibilities</h3>
                        </div>
                        <div className="job-detail-block-content" dangerouslySetInnerHTML={{ __html: currentJob.acf.responsibilities_list }} />
                      </div>
                    </div>
                  </Parallax>
                </div>
                <div className="col-md-6 col-xs-12">
                  <div className="show-on-scroll">
                    <div className="job-detail-block black">
                      <div className="triangle-item" id="requirements-triangle">
                        <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
                          <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                      l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                      C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                      c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                      l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                      c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                      c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                      c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                      C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                      c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
                        </svg>
                      </div>
                      <div className="job-detail-block-title">
                        <h3 className="title-3">Requirements</h3>
                      </div>
                      <div className="job-detail-block-content" dangerouslySetInnerHTML={{ __html: currentJob.acf.requirements_list }} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="section interview-section">
          <div className="triangle-item" id="interview-triangle">
            <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
              <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                    l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                    C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                    c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                    l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                    c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                    c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                    c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                    C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                    c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
            </svg>
          </div>
          <div className="show-on-scroll">
            <div className="job-challenge-preview">
              <Img fluid={currentJob.acf.interview_process_preview.localFile.childImageSharp.fluid}
                   className="job-challenge-preview-image"
                   objectPosition="50% 100%" />
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-md-11 col-md-offset-1">
                <Parallax y={[20, -20]}>
                  <div className="show-on-scroll">
                    <div className="job-challenge-block">
                      <div className="triangle-item" id="job-interview-triangle">
                        <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
                          <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                        l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                        C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                        c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                        l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                        c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                        c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                        c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                        C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                        c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
                        </svg>
                      </div>
                      <div className="row">
                        <div className="col-md-4 col-xs-12">
                          <div className="subtitle white">
                            <p dangerouslySetInnerHTML={{ __html: currentJob.acf.what_to_expert_subtitle }} />
                          </div>
                          <div className="job-challenge-block-title" dangerouslySetInnerHTML={{ __html: currentJob.acf.interview_process_title }} />
                          <div className="job-challenge-block-description">
                            <p dangerouslySetInnerHTML={{ __html: currentJob.acf.interview_process_description }} />
                          </div>
                        </div>
                        <div className="col-md-8 col-xs-12">
                          <div className="job-challenge-block-content" dangerouslySetInnerHTML={{ __html: currentJob.acf.interview_process_content }} />
                        </div>
                      </div>
                    </div>
                  </div>
                </Parallax>
              </div>
            </div>
          </div>
        </section>
        <section className="section job-apply-section">
          <div className="triangle-item" id="job-apply-triangle">
            <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
              <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                    l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                    C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                    c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                    l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                    c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                    c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                    c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                    C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                    c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
            </svg>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-md-6 col-xs-12">
                <Parallax y={[-10, 10]}>
                  <div className="show-on-scroll">
                    <div className="solution-block">
                      <div className="subtitle violet">
                        <p dangerouslySetInnerHTML={{ __html: currentJob.acf.how_to_apply_subtitle }} />
                      </div>
                      <div className="solution-block-title" dangerouslySetInnerHTML={{ __html: currentJob.acf.apply_solution_title }} />
                      <div className="solution-block-description">
                        <p dangerouslySetInnerHTML={{ __html: currentJob.acf.apply_solution_decription }} />
                      </div>
                    </div>
                  </div>
                </Parallax>
              </div>
              <div className="col-md-6 col-xs-12">
                <Parallax y={[10, -20]}>
                  <div className="show-on-scroll">
                    <div className="job-detail-block black">
                      <div className="triangle-item" id="apply-triangle">
                        <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
                          <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                        l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                        C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                        c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                        l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                        c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                        c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                        c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                        C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                        c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
                        </svg>
                      </div>
                      <div className="job-detail-block-title">
                        <h5 className="title-5" dangerouslySetInnerHTML={{ __html: currentJob.acf.applicants_list_title }} />
                      </div>
                      <div className="job-detail-block-content" dangerouslySetInnerHTML={{ __html: currentJob.acf.applicants_list }} />
                    </div>
                  </div>
                </Parallax>
              </div>
            </div>
          </div>
        </section>
        <FooterContact />
        {/* <Footer /> */}
      </Layout>
    )

  }

}

export default JobTemplate;

export const pageQuery = graphql`
    query($id: String!) {
        wordpressWpCareersPosts (id: { eq: $id }){
            id,
            title,
            slug,
            featured_media{
                localFile{
                    childImageSharp{
                        fluid (quality:100, maxWidth: 2000){
                            aspectRatio,
                            sizes,
                            src,
                            srcSet
                        }
                    }
                }
            }
            acf{
                main_title,
                slogan,
                folder_description,
                about_alpha_alias_subtitle,
                about_solution_title,
                about_solution_description,
                about_solution_preview{
                    localFile{
                        childImageSharp{
                            fluid (quality:100, maxWidth: 2000){
                                aspectRatio,
                                sizes,
                                src,
                                srcSet
                            }
                        }
                    }
                },
                about_the_job_subtitle,
                challenge_title,
                challenge_description,
                challenge_content,
                challenge_preview{
                    localFile{
                        childImageSharp{
                            fluid (quality:100, maxWidth: 2000){
                                aspectRatio,
                                sizes,
                                src,
                                srcSet
                            }
                        }
                    }
                },
                area_of_operation_subtitle,
                solution_title,
                solution_description,
                solution_preview {
                    localFile{
                        childImageSharp{
                            fluid (quality:100, maxWidth: 2000){
                                aspectRatio,
                                sizes,
                                src,
                                srcSet
                            }
                        }
                    }
                },
                responsibilities_list,
                requirements_list,
                what_to_expert_subtitle,
                interview_process_title,
                interview_process_description,
                interview_process_content,
                interview_process_preview{
                    localFile{
                        childImageSharp{
                            fluid (quality:100, maxWidth: 2000){
                                aspectRatio,
                                sizes,
                                src,
                                srcSet
                            }
                        }
                    }
                },
                how_to_apply_subtitle,
                apply_solution_title,
                apply_solution_decription,
                applicants_list_title,
                applicants_list
            }
        }
        allWordpressAcfOptions{
            edges{
                node{
                    options{
                        running_text_title
                    }
                }
            }
        }
    }
`