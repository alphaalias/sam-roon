import React, { Component } from 'react'
import { graphql } from "gatsby"
import Link from 'gatsby-plugin-transition-link'
import Img from 'gatsby-image'
import { FacebookShareButton, TwitterShareButton, LinkedinShareButton} from 'react-share'
import { Parallax, ParallaxProvider } from 'react-scroll-parallax'
import SEO from "../components/seo"
import ScrollControl from "../components/scrollControl"
import Header from "../components/header"
import FooterContact from "../components/footerContact"
import Footer from "../components/footer"
import TimeLeftContainer from "../components/timeLeftContainer"
import  Parser from 'html-react-parser'


class New extends Component {

  render() {

    const { wordpressWpNewsPosts } = this.props.data;
    const media = this.props.data.allWordpressWpMedia.edges;
    const shareUrl = this.props.location.href;

    const Content = Parser(wordpressWpNewsPosts.content, {

      replace: domNode => {

        if(domNode.name === 'img') {

          let image = media.filter(m => {

            return m.node.source_url === domNode.attribs.src;

          });

          if(!!image.length) {

            image = image[0].node.localFile;

            return <Img fluid={image.childImageSharp.fluid} />

          }

        }

      }

    });

    const ContentBottom = Parser(wordpressWpNewsPosts.acf.content_bottom, {

      replace: domNode => {

        if(domNode.name === 'img') {

          let image = media.filter(m => {

            return m.node.source_url === domNode.attribs.src;

          });

          if(!!image.length) {

            image = image[0].node.localFile;

            return <Img fluid={image.childImageSharp.fluid} />

          }

        }

      }

    });

    return(
      <main id="wrapper" className="menu-color">
        <TimeLeftContainer
          categoryLink={wordpressWpNewsPosts.categories ? wordpressWpNewsPosts.categories[0].slug : null}
          time={wordpressWpNewsPosts.acf.approximate_reading_time} />
        <Header siteTitle={wordpressWpNewsPosts.title} violetTheme="true" location={this.props.location} />
        <ScrollControl />
        <SEO title={`Alpha Alias - The Project First Agency | ${ (wordpressWpNewsPosts.title).replace(/&#(\d{0,4});/g, function(fullStr, str) { return String.fromCharCode(str); }) }`} description={ wordpressWpNewsPosts.acf.article_seo_description } />
        <ParallaxProvider>
          <section className="section folder __article-folder" id="white-switch-section">
            <div className="triangle-item" id="article-folder-triangle">
              <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
                <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                      l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                      C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                      c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                      l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                      c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                      c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                      c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                      C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                      c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
              </svg>
            </div>
            <div className="folder-bg-wrap">
              <div className="triangle-wrap">
                <div className="triangle-item" id="folder-bg-triangle">
                  <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
                    <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                      l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                      C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                      c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                      l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                      c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                      c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                      c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                      C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                      c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
                  </svg>
                </div>
              </div>
            </div>
            <div className="container">
              <div className="row">
                <div className="col-md-6 col-xs-12">
                  <div className="folder-content-wrap">
                    <div className="folder-content">
                      <div className="bread-crumbs-wrap">
                        <Link
                          rel="canonical"
                          exit={{
                            length: 0.3
                          }}
                          entry={{
                            delay: 0.5
                          }}
                          to="/blog/"
                          className="bread-crumb-link">Blog /</Link>
                        {/*{*/}
                          {/*wordpressWpNewsPosts.categories*/}
                            {/*?*/}
                            {/*<Link*/}
                              {/*rel="canonical"*/}
                              {/*exit={{*/}
                                {/*length: 0.3*/}
                              {/*}}*/}
                              {/*entry={{*/}
                                {/*delay: 0.5*/}
                              {/*}}*/}
                              {/*to={`/blog/${wordpressWpNewsPosts.categories[0].slug}/`}*/}
                              {/*className="bread-crumb-link">*/}
                              {/*{wordpressWpNewsPosts.categories[0].slug} /</Link>*/}
                            {/*: ''*/}
                        {/*}*/}
                        <p className="bread-crumb-link"><span dangerouslySetInnerHTML={{ __html: wordpressWpNewsPosts.title }} /> /</p>
                      </div>
                      <h2 className="title-2 article-title" dangerouslySetInnerHTML={{ __html: wordpressWpNewsPosts.title }} />
                      <Link
                        rel="canonical"
                        exit={{
                          length: 0.3
                        }}
                        entry={{
                          delay: 0.5
                        }}
                        to={`/blog/${wordpressWpNewsPosts.categories[0].slug}/`}
                        className="btn-link __border-theme __violet-theme __sm-theme">
                        <span className="btn-link-title">{ wordpressWpNewsPosts.categories[0].slug }</span>
                      </Link>
                    </div>
                  </div>
                </div>
                <div className="col-md-6 col-xs-12">
                  <div className="folder-main-preview">
                    { wordpressWpNewsPosts.featured_media
                      ?
                      <Img fluid={wordpressWpNewsPosts.featured_media.localFile.childImageSharp.fluid} objectPosition="50% 0" className="folder-image" />
                      : '' }
                  </div>
                </div>
              </div>
            </div>
            <div className="folder-share-wrap">
              <div className="share-list">
                <LinkedinShareButton url={shareUrl} />
                <FacebookShareButton url={shareUrl} />
                <TwitterShareButton url={shareUrl} />
              </div>
              <p className="article-date">Published on {wordpressWpNewsPosts.date}</p>
            </div>
          </section>
          <section className="section article-section">
            <div className="triangle-item" id="article-triangle">
              <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
                <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                      l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                      C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                      c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                      l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                      c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                      c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                      c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                      C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                      c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
              </svg>
            </div>
            <div className="container">
              <div className="row">
                <div className="col-md-10 col-md-offset-1 col-xs-12">
                  <div className="show-on-scroll">
                    {/*<div className="article-content __top-content" dangerouslySetInnerHTML={{ __html: wordpressWpNewsPosts.content }} />*/}
                    <div className="article-content __top-content">{ Content }</div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section className="section article-challenge-section">
            <div className="show-on-scroll">
              <div className="job-challenge-preview">
                {wordpressWpNewsPosts.acf.article_challenge_preview.localFile
                  ?
                  <Img fluid={wordpressWpNewsPosts.acf.article_challenge_preview.localFile.childImageSharp.fluid}
                       className="job-challenge-preview-image"
                       objectPosition="50% 100%" />
                  : ''}
              </div>
            </div>
            <div className="container">
              <div className="row">
                <div className="col-md-11 col-md-offset-1 col-xs-12">
                  <Parallax y={[10, -10]}>
                    <div className="show-on-scroll">
                      <div className="job-challenge-block">
                        <div className="triangle-item" id="job-interview-triangle">
                          <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
                            <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                          l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                          C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                          c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                          l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                          c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                          c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                          c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                          C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                          c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
                          </svg>
                        </div>
                        <div className="row">
                          <div className="col-md-5 col-xs-12">
                            <div className="subtitle white">
                              <p dangerouslySetInnerHTML={{ __html: wordpressWpNewsPosts.acf.article_challenge_main_subtitle }} />
                            </div>
                            <div className="job-challenge-block-title" dangerouslySetInnerHTML={{ __html: wordpressWpNewsPosts.acf.article_challenge_title }} />
                          </div>
                          <div className="col-md-7 col-xs-12">
                            <div className="job-challenge-block-content" dangerouslySetInnerHTML={{ __html: wordpressWpNewsPosts.acf.article_challenge_content }} />
                          </div>
                        </div>
                      </div>
                    </div>
                  </Parallax>
                </div>
              </div>
            </div>
          </section>
          <section className="section article-section __bottom">
            <div className="triangle-item" id="article-triangle-bottom">
              <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
                <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                      l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                      C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                      c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                      l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                      c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                      c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                      c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                      C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                      c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
              </svg>
            </div>
            <div className="triangle-item" id="job-apply-triangle">
              <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
                <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                      l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                      C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                      c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                      l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                      c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                      c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                      c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                      C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                      c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
              </svg>
            </div>
            <div className="container">
              <div className="row">
                <div className="col-md-10 col-md-offset-1 col-xs-12">
                  <div className="show-on-scroll">
                    {/*<div className="article-content" dangerouslySetInnerHTML={{ __html: wordpressWpNewsPosts.acf.content_bottom }} />*/}
                    <div className="article-content">{ ContentBottom }</div>
                  </div>
                  <div className="show-on-scroll">
                    {
                      wordpressWpNewsPosts.tags
                      ?
                        <div className="tags-list">
                          {wordpressWpNewsPosts.tags.map((tag, key) => (
                            <Link
                              rel="canonical"
                              exit={{
                                length: 0.3
                              }}
                              entry={{
                                delay: 0.5
                              }}
                              to={`/blog/tags/${tag.slug}/`}
                              className="tag-item" key={`tag-${key}`}
                            >{tag.slug}</Link>
                          ))}
                        </div>
                      :
                      null
                    }
                  </div>
                  <div className="show-on-scroll">
                    {
                      wordpressWpNewsPosts.acf.article_author_photo
                        ?
                        <div className="article-author-wrap">
                          <div className="article-author-preview">
                            <div className="article-author-photo">

                                  <Img fluid={wordpressWpNewsPosts.acf.article_author_photo.localFile.childImageSharp.fluid} className="article-author-photo-image" />
                            </div>
                            <p className="article-author-position" dangerouslySetInnerHTML={{ __html: wordpressWpNewsPosts.acf.article_author_position }} />
                            <a
                              href={wordpressWpNewsPosts.acf.article_author_social_link}
                              target="_blank"
                              rel="noopener noreferrer"
                              className="article-author-soc-link"> </a>
                          </div>
                          <div className="article-author-content">
                            <div className="subtitle violet">
                              <p>The Author</p>
                            </div>
                            <div className="article-author-title">
                              <h2 className="title-2" dangerouslySetInnerHTML={{ __html: wordpressWpNewsPosts.acf.article_author_title }} />
                            </div>
                            <div className="article-author-description">
                              <p dangerouslySetInnerHTML={{ __html: wordpressWpNewsPosts.acf.article_author_description }} />
                            </div>
                          </div>
                        </div>
                        : null
                    }
                  </div>
                </div>
              </div>
            </div>
          </section>
        </ParallaxProvider>
        {/* <FooterContact /> */}
        <Footer data={wordpressWpNewsPosts}/>
      </main>
    )

  }

}

export default New

export const pageQuery = graphql`
    query($id: String!){
        wordpressWpNewsPosts(id: { eq: $id }) {
          id,
          slug,
          date (formatString: "MMMM DD, YYYY"),
          title,
          content,
          featured_media{
              localFile{
                  childImageSharp{
                      fluid(quality:100){
                          aspectRatio,
                          src,
                          sizes,
                          srcSet
                      }
                  }
              }
          }
          categories{
              slug
          }
          tags{
              slug
          }
          acf{
              article_seo_description,
              content_bottom,
              approximate_reading_time,
              article_challenge_main_subtitle,
              article_challenge_title,
              article_challenge_content,
              next_step
                  next_step_links {
                    step_name
                    step_link
                },
              article_challenge_preview{
                  localFile{
                      childImageSharp{
                          fluid (quality:100, maxWidth: 2000){
                              aspectRatio,
                              src,
                              sizes,
                              srcSet
                          }
                      }
                  }
              }
              article_author_title,
              article_author_position,
              article_author_description,
              article_author_social_link,
              article_author_photo{
                  localFile{
                      childImageSharp{
                          fluid (quality:100, maxWidth: 100){
                              aspectRatio,
                              src,
                              sizes,
                              srcSet
                          }
                      }
                  }
              }
          }
        }
        allWordpressWpMedia{
            edges{
                node{
                    source_url,
                    localFile{
                        childImageSharp{
                            fluid(quality:100){
                                aspectRatio,
                                src,
                                sizes,
                                srcSet
                            }
                        }
                    }
                }
            }
        }
}`