import React  from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql } from "gatsby"
import { PopupboxContainer } from 'react-popupbox'
import { ParallaxProvider } from 'react-scroll-parallax'

import Header from "./header"
import "../styles/global.scss"

const popupboxConfig = {
  titleBar: {
    enable: true
  },
  fadeIn: true,
  fadeInSpeed: 500
}

const Layout = ({ children, location, theme }) => (

  <StaticQuery
    query={GET_META}
    render={data => (
      <div>
        <main id="wrapper">
          <Header siteTitle={data.site.siteMetadata.title} location={location} theme={theme} />
          <ParallaxProvider className="page-content">
            {children}
          </ParallaxProvider>
          <PopupboxContainer { ...popupboxConfig } />
        </main>
      </div>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout

const GET_META = graphql`
    query SiteTitleQuery {
        site {
            siteMetadata {
                title
            }
        }
    }
`