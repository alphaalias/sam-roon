import React, { Component } from 'react'
import FooterNav from "./footerNav"
import { graphql, StaticQuery } from 'gatsby'

class FooterNav2 extends Component {

  render() {

    const data = this.props.data;

    return(
      
        
        <div className="footer-content-panel-item">
          
          <StaticQuery
            query={GET_FOOTER_MENU2}
            render={data => <FooterNav data={data} />}
          />
        </div>
        
      
    )

  }

}

export default FooterNav2

const GET_FOOTER_MENU2 = graphql`
    {
        wordpressWpApiMenusMenusItems (id: { eq: "14fd2017-c1bf-5486-a3d4-8ed5e3d8ff2c" }){
            items{
                title,
                url
            }
        }
    }
`