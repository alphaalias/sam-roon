import React, { Component } from 'react'
import Link from 'gatsby-plugin-transition-link'
import PropTypes from "prop-types"

let timeLeft;
let timeProgress;

class TimeLeftContainer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      visibleMessage: false,
      leftTime: 0
    }
  }

  componentDidMount() {

    timeLeft = document.getElementById('time-left-wrap');
    timeProgress = document.getElementById('time-left-progress');

    window.addEventListener('scroll', this.handleScroll);

  }

  componentWillUnmount() {

    window.removeEventListener('scroll', this.handleScroll);

  }

  handleScroll = () => {

    const { time } = this.props;

    let scrollRate = Math.round((window.scrollY / (document.documentElement.scrollHeight -  window.innerHeight)) * 100);

    this.setState({
      leftTime: time - Math.round((time * (scrollRate / 100)))
    });

    timeProgress.style.width = scrollRate + '%';

    if(this.state.leftTime <= 0) {

      setTimeout(function() {

        timeLeft.style.transform = 'translate(0px,0px)';
        timeLeft.style.transition = 'transform .4s';

      }, 1000);

    } else {

      timeLeft.style.transform = 'translate(0px,-'+ Math.min(100, window.scrollY) +'%)';
      timeLeft.style.transition = 'transform .1s'

    }

  }

  render() {

    const { leftTime } = this.state;
    const { categoryLink } = this.props;

    return(
      <div className="time-left-wrap" id="time-left-wrap">
        <div className="time-left-progress" id="time-left-progress"></div>
        <div className="time-left-panel">
          {
            categoryLink
            ?
              <Link
                rel="canonical"
                exit={{
                  length: 0.3
                }}
                entry={{
                  delay: 0.5
                }}
                to={`/blog/${categoryLink}/`}
                className="btn-link __border-theme __white-theme __dynamic-width"
              >Back To Category Blog Page</Link>
            : null
          }
          <p className="time-left-message">{ leftTime } min</p>
          <Link
            rel="canonical"
            exit={{
              length: 0.3
            }}
            entry={{
              delay: 0.5
            }}
            to={`/`}
            className="btn-link __border-theme __white-theme __dynamic-width"
          >Back To Home Page</Link>
        </div>
      </div>
    )

  }

}

TimeLeftContainer.propTypes = {
  categoryLink: PropTypes.string.isRequired,
  time: PropTypes.string.isRequired
}

export default TimeLeftContainer