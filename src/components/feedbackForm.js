import React, { Component } from 'react'
import { PopupboxManager } from 'react-popupbox'
import logo from '../images/ALPHAALIASV.svg'
import axios from 'axios'
import PropTypes from 'prop-types'
import InputMask from 'react-input-mask'
import ReCAPTCHA from "react-google-recaptcha"

const defaultState = {
  action: 'siteWideMessage',
  name: '',
  email: '',
  phone: '',
  message: '',
  nameError: '',
  emailError: '',
  phoneError: '',
  messageError: '',
  captchaValue: '',
  captchaError: ''
};

class FeedbackForm extends Component {

  constructor(props) {
    super(props);
    this.state = defaultState;
  }

  handleChangeValue = (event) => {

    this.setState({
      [event.target.name]: event.target.value
    });

  }

  handleClosePopup = () => {

    PopupboxManager.close();

  }

  handleSubmit = (event) => {

    event.preventDefault();

    const content = (
      <div>
        <div className="popup-logo">
          <img src={logo} alt="Thank You!" />
        </div>
        <h4 className="title-4">Thank You!</h4>
        <p>Your message has been successfully sent!</p>
        <button className="btn-link __solid-theme" onClick={this.handleClosePopup}>
          <span className="btn-link-title">Ok</span>
        </button>
      </div>
    );

    if(this.handleValidForm()) {

      const { name, email, phone, message } = this.state;

      axios({
        method: 'GET',
        url: '../server/sendEmail.php',
        params: {
          name: name,
          email: email,
          phone: phone,
          message: message
        }
      })
        .then(() => {

          PopupboxManager.open({ content })

          this.setState(defaultState);

        })
        .catch(error => {

          console.log(error);

        });

    }

  }

  handleValidForm = () => {

    const { name, email, phone, message, captchaValue } = this.state;

    let nameError = '';
    let emailError = '';
    let phoneError = '';
    let messageError = '';
    let captchaError = '';
    // let regex = new RegExp(/[^0-9]/, 'g');

    if(name.length <= 0) {

      nameError = 'You missed this field'

    }

    if(email.length <= 0) {

      emailError = 'You missed this field';

    } else if(!email.includes('@')) {

      emailError = 'This field is not correct';

    }

    if(phone.length <= 0) {

      phoneError = 'You missed this field';

    }

    if(message.length <= 0) {

      messageError = 'You missed this field'

    }

    if(captchaValue.length <= 0) {

      captchaError = 'You missed this field'

    }

    if(nameError || emailError || phoneError || messageError || captchaError) {

      this.setState({
        nameError,
        emailError,
        phoneError,
        messageError,
        captchaError
      });

      return false

    }

    return true

  }

  handleCaptchaChange = (value) => {

    this.setState({
      captchaValue: value
    })

  }

  render() {

    const { name, email, phone, message, nameError, emailError, phoneError, messageError, captchaError } = this.state;

    const acf = this.props.data.acf;

    return(
      <div className="feedback-form">
        <div className="form-wrap">
          <form id="feedback-form" onSubmit={this.handleSubmit}>
            <div className={`input-container ${Boolean(nameError) ? 'error' : ''}`}>
              <input
                type="text"
                name="name"
                className="input-item"
                placeholder={acf.name_input_placeholder}
                value={name}
                onChange={this.handleChangeValue}/>
              <div className="input-message">{nameError}</div>
            </div>
            <div className={`input-container ${Boolean(emailError) ? 'error' : ''}`}>
              <input
                type="text"
                name="email"
                className="input-item"
                placeholder={acf.e_mail_input_placeholder}
                value={email}
                onChange={this.handleChangeValue}/>
              <div className="input-message">{emailError}</div>
            </div>
            <div className={`input-container ${Boolean(phoneError) ? 'error' : ''}`}>
              <InputMask
                name="phone"
                className="input-item"
                placeholder={acf.phone_input_placeholder}
                value={phone}
                onChange={this.handleChangeValue}
                mask="+999 99 9999999"
                type="tel"
              />
              <div className="input-message">{phoneError}</div>
            </div>
            <div className={`input-container ${Boolean(messageError) ? 'error' : ''}`}>
              <textarea
                name="message"
                cols="10"
                rows="3"
                className="textarea-item"
                placeholder={acf.message_input_placeholder}
                value={message}
                onChange={this.handleChangeValue}></textarea>
              <div className="input-message">{messageError}</div>
            </div>
            <div className="input-container">
              <ReCAPTCHA
                sitekey="6Lc1MawUAAAAAH1MuU_G17eLVONCFvS2hUCRPOfv"
                onChange={this.handleCaptchaChange}
              />
              <div className="input-message captcha-message">{captchaError}</div>
            </div>
            <div className="form-action">
              <button className="btn-link __solid-theme">
                <span className="btn-link-title" dangerouslySetInnerHTML={{ __html: acf.feedback_button_title }} />
              </button>
            </div>
          </form>
        </div>
      </div>
    )

  }

}

FeedbackForm.propTypes = {
  data: PropTypes.object.isRequired
};

export default FeedbackForm