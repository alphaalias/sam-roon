import React, { Component } from 'react'
import { StaticQuery, graphql } from 'gatsby'
import ClientsCarousel from "./clientsCarousel"

class Clients extends Component{

  render() {

    return (
      <section className="section clients">
        <div className="container">
          <StaticQuery query={GET_CLIENTS} render={ data => <ClientsCarousel clients={data.allWordpressAcfOptions.edges[0].node.options} /> } />
        </div>
      </section>
    )

  }

}

export default Clients

const GET_CLIENTS = graphql`
    {
        allWordpressAcfOptions{
            edges{
                node{
                    options{
                        clients_list_title,
                        clients_list_subtitle,
                        clients_list{
													logo{
                            localFile{
                              childImageSharp{
                                fluid{
                                  aspectRatio,
                                  src,
                                  sizes,
                                  srcSet
                                }
                              }
                            }
                          },
                          title,
                          site_url
                        }
                    }
                }
            }
        }
    }
`