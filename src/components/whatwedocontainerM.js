import React, { Component } from 'react'
import { graphql, StaticQuery } from 'gatsby'
import WhaWeDoP from "./whawedoP"

class WhatWeDoContainer extends Component {

  render() {

    return(
      <StaticQuery
        query={GET_WHAT_WE_DO}
        render={data => <WhaWeDoP data={data} />}
      />
    )

  }

}

export default WhatWeDoContainer

const GET_WHAT_WE_DO = graphql`
    {
        wordpressPage (id: { eq: "bd53a969-63c5-5ffc-a34b-e0bded3b4dfd" }) {
            title,
            acf{
                what_we_do_title,
                what_we_do_slogan,
                what_we_do_description,
                what_we_do_cta_title,
                what_we_do_cta_url,
                what_we_do_preview{
                    localFile{
                        childImageSharp{
                            fluid (maxWidth: 2000){
                                aspectRatio,
                                sizes,
                                src,
                                srcSet
                            }
                        }
                    }
                }
                what_we_do_list{
                    title,
                    description,
                    icon{
                        localFile{
                            publicURL
                        }
                    }
                }
            }
        }
    }
`