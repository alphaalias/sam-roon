import React, { Component } from 'react'
import { graphql, StaticQuery } from 'gatsby'
import SocialsItem from "./socialsItem"

class Socials extends Component {

  render() {

    return(
      <div className="social-wrap">
        <StaticQuery
          query={GET_SOCIALS}
          render={data => (
            <SocialsItem socialData={data} />
          )}
        />
      </div>
    )

  }

}

export default Socials

const GET_SOCIALS = graphql`
    {
        allWordpressAcfOptions{
            edges{
                node{
                    options{
                        social_list{
                            url,
                            social_icon_white {
                                source_url,
                                localFile{
                                    publicURL
                                }
                            },
                            social_icon_black {
                                source_url,
                                localFile{
                                    publicURL
                                }
                            }
                        }
                    }
                }
            }
        }
    }
`