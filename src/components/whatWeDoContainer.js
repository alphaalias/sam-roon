import React, { Component } from 'react'
import { graphql, StaticQuery } from 'gatsby'
import WhaWeDo from "./whaWeDo"

class WhatWeDoContainer extends Component {

  render() {

    return(
      <StaticQuery
        query={GET_WHAT_WE_DO}
        render={data => <WhaWeDo data={data} />}
      />
    )

  }

}

export default WhatWeDoContainer

const GET_WHAT_WE_DO = graphql`
    {
        wordpressPage (id: { eq: "ada0b2fd-92db-575c-a771-9c0b162278ab" }) {
            title,
            acf{
                what_we_do_title,
                what_we_do_slogan,
                what_we_do_description,
                what_we_do_cta_title,
                what_we_do_cta_url,
                what_we_do_preview{
                    localFile{
                        childImageSharp{
                            fluid (maxWidth: 2000){
                                aspectRatio,
                                sizes,
                                src,
                                srcSet
                            }
                        }
                    }
                }
                what_we_do_list{
                    title,
                    description,
                    icon{
                        localFile{
                            publicURL
                        }
                    }
                }
            }
        }
    }
`