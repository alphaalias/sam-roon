import React, { Component } from 'react'
import { graphql, StaticQuery } from 'gatsby'
import Link from 'gatsby-plugin-transition-link'

import logo from '../images/logo-white.svg'
import FooterData from "./footerData"
import FooterAddress from './footerAddress'
import Socials from "./socials"
import mgif from '../images/moshed.gif'

class FooterContact extends Component{

  render() {

    return(
      <footer className="footer mobile-chtbot" id="footer">
        <div className="footer-content">
        <div className="container-fuild">
          <div className="next-step-sec">
          <div className="inspired-preview test-white">
           
            </div>
          </div>
          <div className="chat-bot-row mobile-chat" id="mob-chat-bot">
                 <div className="cht-title"> <h5 class="title-5 black">Pick a Time to Chat</h5></div>
              <iframe class="meetings-iframe-container" title="Pick a Time to Chat" src="https://meetings.hubspot.com/sam1174?embed=true" width="350" height="100%" frameborder="0"></iframe>
                </div>
          </div>
          <div className="container">
            {/* <div className="triangle-item" id="footer-triangle">
              <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
                <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                  l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                  C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                  c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                  l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                  c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                  c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                  c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                  C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                  c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
              </svg>
            </div> */}
            <div className="row">
              <div className="col-md-6 col-sm-6 col-xs-12" id="chat-deskfull">
                <div className="row">              
                  <div className="col-sm-12 col-xs-12">
                    <div className="row">
                    <FooterData/>
                    
                    </div>
                  </div>
                </div>
                <div className="social-row" id="social-items">
                  <div className="row">
                    <div className="col-xs-12">
                      <div className="footer-content-panel-item">
                        {/* <p className="footer-nav-title">Social Networks</p> */}
                        <Socials />
                      </div>
                    </div>
                  </div>
                </div>
              <div className="copyright-wrap">
              <div className="">
                <div className="row">
                  <div className="col-md-3 col-xs-12 col-sm-3">
                    <StaticQuery
                        query={GET_FOOTER_CONTACTS}
                        render={data => <FooterAddress data={data} />}
                      />
                  </div>
                  <div className="col-md-5 col-xs-12 col-sm-6">
                    <div className="copy-area">
                    <Link
                        exit={{
                          length: 0
                        }}
                        entry={{
                          delay: 0
                        }}
                        to="/"
                        className="footer-logo">
                        <img src={logo} alt="" />
                      </Link>
                      <p className="copyright-title">Copyright &copy; 2021 All rights reserved.</p>
                      </div>
                  </div>
                  <div className="col-md-4 col-xs-12 single-link-footer col-sm-3">
                  <Link to="/privacy"
                        className="footer-link-single">
                        Privacy Policy
                      </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
              <div className="col-md-6 col-sm-6 col-xs-12" id="chat-desknone">
              <div className="footer-bg-2"></div>
                <div className="chat-bot-row deskchat" id="chat-title">
                  <h5 class="title-5 black" id="chat-title">Pick a Time to Chat</h5>
              <iframe class="meetings-iframe-container" title="Pick a Time to Chat" src="https://meetings.hubspot.com/sam1174?embed=true" width="700" height="670" frameborder="0"></iframe>
                </div>
              </div>
            </div>
          </div>
          <img className="fp-gif" src={mgif} alt="" />
        </div>
        
        
      </footer>
    )

  }

}

export default FooterContact

const GET_FOOTER_CONTACTS = graphql`
    {
      allWordpressAcfOptions{
        edges{
            node{
                options{
                    email,
                    phone_number,
                    address
                }
            }
        }
    }
    }
`