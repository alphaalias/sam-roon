import React, { Component } from 'react'
// import Socials from "./socials"
import FooterNav from "./footerNav"
import { graphql, StaticQuery } from 'gatsby'

import FooterNav2 from './footerNav2';
import FooterNav3 from './footerNav3';
import FooterNav4 from './footerNav4';
class FooterData extends Component {

  render() {

    const data = this.props.data;

    return(
      <div className="footer-content-panel">
        <div className="col-md-3 col-xs-6">
          
            <StaticQuery
              query={GET_FOOTER_MENU}
              render={data => <FooterNav data={data} />}
            />
         
        </div>
        <div className="col-md-3 col-xs-6">
          
          <FooterNav2/>
          
        </div>
        <div className="col-md-3 col-xs-6">
         
          <FooterNav3/>
         
        </div>
        <div className="col-md-3 col-xs-6">
          
          <FooterNav4/>
         
        </div>
      </div>
    )

  }

}

export default FooterData

const GET_FOOTER_MENU = graphql`
    {
        wordpressWpApiMenusMenusItems (id: { eq: "6f8fa716-5938-5bcb-9b50-2fe6a5e790dd" }){
            items{
                title,
                url
            }
        }
    }
    
`
