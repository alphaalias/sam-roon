import React, { Component } from 'react'
import Link from 'gatsby-plugin-transition-link'
import PropTypes from 'prop-types'

class FooterNav extends Component {

  render() {

    const footerMenu = this.props.data.wordpressWpApiMenusMenusItems.items;

    return(
      
      <div className="footer-nav">
        <ul className="footer-nav-list">
          {
            footerMenu.map((item, key) => (
              <li className="footer-nav-list-item" key={key}>
                <Link
                  rel="canonical"
                  exit={{
                    length: 0.3
                  }}
                  entry={{
                    delay: 0.5
                  }}
                  to={`${ item.url }/`}
                  className="nav-link">{ item.title }</Link>
              </li>
            ))
          }
        </ul>
      </div>
      
    )

  }

}

export default FooterNav

FooterNav.propTypes = {
  data: PropTypes.object.isRequired
};