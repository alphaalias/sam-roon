import React, { Component } from 'react'

class SocialsItem extends Component {

  render() {

    const socialList = this.props.socialData.allWordpressAcfOptions.edges[0].node.options.social_list;

    return(
      <ul className="social-wrap-list">
        {
          socialList.map((socialLink, key) => {
            return (
              <li key={key} className="social-wrap-list-item">
                <a href={socialLink.url} className="social-link" target="_blank" rel="noopener noreferrer">
                  <img src={socialLink.social_icon_black.localFile.publicURL} alt="" className="black-icon"/>
                  <img src={socialLink.social_icon_white.localFile.publicURL} alt="" className="white-icon"/>
                </a>
              </li>
            )
          })
        }
      </ul>

    )

  }

}

export default SocialsItem