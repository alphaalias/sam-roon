import React, { Component } from "react"
import PropTypes from "prop-types"
import Link from 'gatsby-plugin-transition-link'
import { graphql, StaticQuery } from 'gatsby'


import logo from '../images/logo-white.svg'
import logoViolet from '../images/logo-violet.svg'
import MainNav from "./mainNav"

class Header extends Component {

  constructor(props) {

    super(props);
    this.state = {
      isOpening: false,
      pathName: '',
      isFixed: false,
      isHiddenHeader: false,
      lastScrollPos: 0,
      whiteLogo: true
    }

  }

  componentDidMount() {

    const { violetTheme } = this.props;

    setTimeout(this.handleScroll(), 1000);
    window.addEventListener('scroll', this.handleScroll);

    if(this.props.location) {

      let absPathName = this.props.location.pathname;

      absPathName.indexOf(1);

      absPathName = absPathName.split('/')[1];

      this.setState({
        pathName: absPathName
      })

    }

    violetTheme ? this.setState({whiteLogo: false}) : this.setState({whiteLogo: true})

  }

  componentWillUnmount() {

    window.removeEventListener('scroll', this.handleScroll);

  }

  handleScroll = () => {

    const { isOpening, lastScrollPos } = this.state;
    const { violetTheme } = this.props;

    // TRIANGLES ANIMATION //
    const triangles = document.getElementsByClassName('triangle-path');

    for (let i = 0; i < triangles.length; i++) {

      if((triangles[i].getBoundingClientRect().top - window.innerHeight) < 0) {

        triangles[i].style.strokeDashoffset = 0;

      }

    }

    const hiddenElements = document.getElementsByClassName('show-on-scroll');

    for (let i = 0; i < hiddenElements.length; i++) {

      if((hiddenElements[i].getBoundingClientRect().top - (window.innerHeight / 1.1)) < 0) {

        hiddenElements[i].style.opacity = 1;
        hiddenElements[i].style.transform = 'translate(0,0)';

      }

    }

    if(window.scrollY > 0 && isOpening && violetTheme) {

      this.setState({
        isOpening: false,
        whiteLogo: false
      });

    } else {

      this.setState({
        isOpening: false
      });

    }

    // FIXED HEADER THEME //
    if(window.scrollY >= 1) {

      this.setState({
        isFixed: true
      })

    } else {

      this.setState({
        isFixed: false
      })

    }

    if(lastScrollPos > window.scrollY) {

      this.setState({
        isHiddenHeader: false,
        lastScrollPos: window.scrollY
      })

    } else if(lastScrollPos < window.scrollY) {

      this.setState({
        isHiddenHeader: true,
        lastScrollPos: window.scrollY,
      })

    }

  }

  handleToggleMainMenu = () => {

    const { violetTheme } = this.props;

    if(violetTheme) {

      this.setState({
        isOpening: !this.state.isOpening,
        whiteLogo: !this.state.whiteLogo
      });

    } else {

      this.setState({
        isOpening: !this.state.isOpening
      });


    }

  }

  render() {

    const data = this.props;
    const { isOpening, pathName, isFixed, isHiddenHeader, whiteLogo } = this.state;

    return(
      <header
        className={`header ${ Boolean(data.violetTheme) ? '__violet-theme' : ''} ${isFixed ? '__is-fixed' : ''} ${isHiddenHeader ? '__is-hidden' : ''} ${data.theme === 'black' ? '__black-theme' : ''}` }
        ref={header => this.header = header}
        id="header">
        <div className="header-bg"></div>
        <div className="header-panel">
          <Link
            exit={{
              length: 0.3
            }}
            entry={{
              delay: 0.5
            }}
            to="/"
            className="header-logo"
          >
            <img src={ whiteLogo ? logo : logoViolet } alt={data.siteTitle} />
          </Link>
          <button className={`toggle-menu ${isOpening ? '__active-theme' : ''}`} onClick={this.handleToggleMainMenu}>
            <span className="m-title">Menu</span>
            <span className="m-title-open">Close Menu</span>
            <span className="toggle-menu-wrap">
              <span className="toggle-menu-arrow"></span>
              <span className="toggle-menu-arrow"></span>
              <span className="toggle-menu-arrow"></span>
            </span>
          </button>
          <StaticQuery
            query={GET_MAIN_MENU}
            render={data => <MainNav isOpening={isOpening} handleToggleMainMenu={this.handleToggleMainMenu} pathName={pathName} data={data} />}
          />
        </div>
      </header>
    )

  }

}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header

const GET_MAIN_MENU = graphql`
    {
        wordpressWpApiMenusMenusItems (id: { eq: "e515d06e-ae31-5723-b1b6-d95b10085287" }){
            items{
                title,
                url,
                wordpress_children {
                  title
                  url
                }
            }
        }
    }
`