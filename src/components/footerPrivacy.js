import React, { Component } from 'react'
import { graphql } from 'gatsby'


class Privacy extends Component{

  render() {

    return (
      <section className="section clients">
        <div className="container">
        <div dangerouslySetInnerHTML={{ __html: data.allWordpressAcfOptions.edges[0].node.options.privacy_policy_link }} className="footer-nav-link" />
        </div>
      </section>
    )

  }

}

export default Privacy

const GET_CLIENTS = graphql`
    {
        allWordpressAcfOptions{
            edges{
                node{
                    options{
                        privacy_policy_link                        
                    }
                }
            }
        }
    }
`