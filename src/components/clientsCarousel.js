import React, { Component } from 'react'
import Slider from 'react-slick'
import Img from 'gatsby-image/withIEPolyfill'

class ClientsCarousel extends Component{

  state = {
    windowWidth: 0
  }

  componentDidMount() {

    this.setState({
      windowWidth: window.outerWidth
    });

  }

  onTestimonialPrev = () => {

    this.slider.slickPrev();

  }

  onTestimonialNext = () => {

    this.slider.slickNext();

  }

  render() {

    const { clients: { clients_list, clients_list_title, clients_list_subtitle } } = this.props;
    const { windowWidth } = this.state;
    const clientsCarouselSettings = {
      dots: false,
      arrows: false,
      infinite: true,
      speed: 800,
      slidesToShow: 5,
      slidesToScroll: 1,
      fade: false,
      cssEase: 'cubic-bezier(.7,.5,.6,1)',
      swipeToSlide: true,
      // variableWidth: true
      responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 4
          }
        },
        {
          breakpoint: 576,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 400,
          settings: {
            slidesToShow: 2
          }
        }
      ]
    }

    return(
      <div className="clients-carousel-wrap">
        <div className="subtitle violet">
          <p className="testimonial-wrap-title">{ clients_list_subtitle }</p>
        </div>
        <h2 className="title-2">{ clients_list_title }</h2>
        <Slider {...clientsCarouselSettings}  className="clients-carousel" ref={slider => (this.slider = slider)}>
          {
            clients_list.map((client, key) => {
              return(
                <div className="clients-carousel-slide" key={key}>
                  <a
                    href={client.site_url || null}
                    target="_blank"
                    rel="noopener noreferrer"
                    className="client-item">
                      <Img
                        fluid={client.logo.localFile.childImageSharp.fluid}
                        className="client-item-logo"
                        title={client.title}
                        objectFit="contain"
                        objectPosition="50% 50%" />
                  </a>
                </div>
              )
            })
          }
        </Slider>
        {
          clients_list.length > 5 || windowWidth < 767
            ?
            <div className="custom-carousel-nav-wrap __clients">
              <div className="custom-nav-item __prev" onClick={this.onTestimonialPrev}>
                Last
                <div className="custom-nav-item-arrow"></div>
              </div>
              <div className="custom-nav-item __next" onClick={this.onTestimonialNext}>
                Next
                <div className="custom-nav-item-arrow"></div>
              </div>
            </div>
            : null
        }
      </div>
    )

  }

}

export default ClientsCarousel