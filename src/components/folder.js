import React, { Component } from 'react'
import Img from 'gatsby-image/withIEPolyfill'
import PropTypes from "prop-types"
import { Parallax } from 'react-scroll-parallax'

class Folder extends Component {

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {

    window.removeEventListener('scroll', this.handleScroll);

  }

  handleScroll = () => {

    if(window.innerWidth > 991) {

      const opacityValue = Math.min(1, window.scrollY / 500);

      this.folderContent.style.opacity = 1 - opacityValue;
      this.folderPreview.style.opacity = 1 - opacityValue;

    }

  }

  render() {

    const { data } = this.props;

    return (
      <section className="section folder">
        <div className="triangle-item" id="default-folder-triangle">
          <svg version="1.1" x="0px" y="0px" viewBox="-55 28.4 153 124.2">
            <path className="triangle-path" d="M10.4,45.2C16,45,21.4,43.6,26,40.1l1-0.6l-6.6-10.7L10.4,45.2z M23.7,68.7c5.9-0.8,12-4.1,16.9-7.2l-6.9-11.2
                    l-1.2,0.7c-8.6,6.1-18.5,4.5-27.9,3.6l-7.6,12.4C5,64.3,14.9,69.9,23.7,68.7z M-25.4,132c3.2-2,6.4-3.9,9.6-5.9
                    C-4,118.5-5.2,102.8,2.6,92.2c7.9-10.6,22.5,0.7,33.9-1.7c6.8-1,11.8-3.7,16.1-6.3l1.3-0.8l-6.7-10.9l-1.4,0.8
                    c-4.3,2.7-9.3,5.4-15.8,6.4c-11,2.1-18.6-6.4-28.7-2c-12.2,5.3-11.9,29.7-23.9,37.6c-3.2,2-6.4,3.9-9.6,5.9c-2.2,1.3-4.4,2.7-6.5,4
                    l-14.8,24.1L-25.4,132z M84.1,136.4c-12.2,8.4-25.9,1.4-36.1,1.6c-3.3,0.1-10.3,1.5-13.5,9.7c-0.6,1.4-1,2.9-1.5,4.3h11.8
                    c1.1-1.3,2.4-2.3,3.9-2.7c11.7-3.2,29.1,6,42.1-2c0.6-0.4,1.2-0.7,1.8-1.1l-6.7-10.9L84.1,136.4z M-3,147.2
                    c14.3-7.9,14.8-20.8,17.8-29.4c2.2-6.1,6.6-10.1,16.1-8.8c12,1.6,24.1,3.8,34.5-3.8l1.5-0.9l-6.7-10.9l-1.5,0.9
                    c-20.7,13.3-36.2-0.5-46.1,5.2c-9.9,5.6-8.4,28-22.4,36.8c-3.2,2-6.4,3.9-9.6,5.9c-5.4,3.3-10.8,6.6-16.1,9.9h24.5
                    C-8.3,150.5-5.6,148.9-3,147.2z M28,120.6c-4.2,2-6.8,8.9-8.3,13.3c-2.4,6.9-4.6,12.9-9.6,18.2h14.4c4.2-9.5,5.2-19.1,10.9-21.7
                    c4.8-2.2,10.7-1.5,15.7-0.9c6,0.7,18.5,3.6,27.2-2.8l1.6-1l-6.6-10.7c-0.6,0.3-1.2,0.6-1.8,0.9C52.9,124,38.9,115.5,28,120.6z"/>
          </svg>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-md-5 col-xs-12">
              <div className="folder-content-wrap" ref={folderContent => this.folderContent = folderContent}>
                <Parallax y={[-30, 30]}>
                  <div className="folder-content">
                    { data.acf.logo
                      ?
                      <div className="folder-logo">
                        <Img
                          fluid={data.acf.logo.localFile.childImageSharp.fluid}
                          objectFit="contain"
                          objectPosition="0 50%"
                          className="folder-logo-image" />
                      </div>
                      :
                      '' }
                    <div className="subtitle white">
                      <p dangerouslySetInnerHTML={{ __html: data.acf.slogan }} />
                    </div>
                    <div dangerouslySetInnerHTML={{ __html: data.acf.main_title }} className="title-1" />
                    <div className="description">
                      <p dangerouslySetInnerHTML={{ __html: data.acf.folder_description }} />
                    </div>
                  </div>
                </Parallax>
              </div>
            </div>
          </div>
        </div>
        <div className="folder-main-preview">
          { data.featured_media
            ?
            <div className="folder-preview-wrap" ref={folderPreview => this.folderPreview = folderPreview}>
              <Img
                fluid={data.featured_media.localFile.childImageSharp.fluid}
                objectPosition="50% 0"
                className="folder-image"
              />
              </div>
            : '' }
        </div>
      </section>
    )

  }

}

Folder.propTypes = {
  data: PropTypes.object,
}

export default Folder;