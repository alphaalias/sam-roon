import React, { Component } from 'react'
import PropTypes from 'prop-types'

class RunningText extends Component{

  constructor(props) {
    super(props);
    this.state = {
      minHeight: 0
    };
  }

  componentDidMount() {

    window.addEventListener('scroll', this.handleScroll);
    this.setState({
      minHeight: document.getElementById('running-text').clientHeight
    })

  }

  handleScroll = () => {

    let moveVal = 0;

    if(this.runningText && (this.runningText.getBoundingClientRect().top - window.innerHeight) < 0) {

      moveVal = Math.abs(this.runningText.getBoundingClientRect().top - window.innerHeight);

      // this.runningText.scrollLeft = moveVal
      this.runningText.style.transform = 'translate(-'+ moveVal +'px,0px)';

    }

  }

  render() {

    const { minHeight } = this.state;

    const runningText = this.props.data.edges[0].node.options.running_text_title

    return(
      <section className="section running-text-section show-on-scroll" style={{ minHeight: minHeight}}>
        <div className="running-text-wrap" id="running-text" ref={(runningText) => this.runningText = runningText }>
          <p className="running-text" dangerouslySetInnerHTML={{ __html: runningText }} />
          <p className="running-text" dangerouslySetInnerHTML={{ __html: runningText }} />
          <p className="running-text" dangerouslySetInnerHTML={{ __html: runningText }} />
        </div>
      </section>
    )

  }

}

RunningText.propTypes = {
  data: PropTypes.object.isRequired
};

export default RunningText