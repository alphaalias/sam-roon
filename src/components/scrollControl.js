import React, { Component } from 'react'
import Slider from 'react-input-slider';

class ScrollControl extends Component {

  constructor(props) {

    super(props)
    this.state = {
      startScrolling: false,
      y: 0,
      maxY: 0,
      enableMoveScrollControl: true
    }

  }

  componentDidMount() {

    setTimeout(this.handleScroll, 1000);
    window.addEventListener('scroll', this.handleScroll);

  }

  componentWillUnmount() {

    window.removeEventListener('scroll', this.handleScroll);

  }

  handleScroll = () => {

    if(window.scrollY > 0) {

      this.setState({
        startScrolling: true
      })

    } else {

      this.setState({
        startScrolling: false
      })

    }

    // SCROLL CONTROL ANIMATION //
    const scrollControl = document.getElementById('scroll-control');
    const whiteSwitchSection = document.getElementById('white-switch-section');
    const whiteSwitchBlock = document.getElementById('white-switch-scrollbar');
    const blackSwitchBlock = document.getElementById('black-switch-scrollbar');
    const footer = document.getElementById('footer');
  


    if(whiteSwitchBlock && (whiteSwitchBlock.getBoundingClientRect().top - (window.innerHeight / 2)) < 0 && window.innerWidth < 576) {

      scrollControl.style.color = '#172528'

    } else {

      scrollControl.style.color = '#ffffff'

    }

    if(blackSwitchBlock && (blackSwitchBlock.getBoundingClientRect().top - (window.innerHeight / 2)) < 0 && window.innerWidth < 576) {

      scrollControl.style.color = '#ffffff'

    }

    if((whiteSwitchSection && whiteSwitchSection.getBoundingClientRect().top - (window.innerHeight / 2)) < 0) {

      scrollControl.style.color = '#172528'

    }

    if(footer && (footer.getBoundingClientRect().top - (window.innerHeight / 2)) < 0) {

      scrollControl.style.color = '#ffffff'

    }
    

    // SCROLL CONTROL MOVING //
    let dotMoving = (window.scrollY * 100) / (document.documentElement.scrollHeight -  window.innerHeight);

    this.setSliderLimit(document.documentElement.scrollHeight -  window.innerHeight);
    if(this.state.enableMoveScrollControl) {

      scrollControl.style.top = ((window.innerHeight / 2) + dotMoving) +'px';

    }

    this.setSliderValue(window.scrollY);

  }

  setSliderLimit = (value) => {

    this.setState({
      maxY: value
    })

  }

  setSliderValue = (value) => {

    this.setState({
      y: value,
      enableMoveScrollControl: false
    })

  }

  handleChangeSlider = (e) => {

    this.setState({
      y: e.y
    })

    document.documentElement.scrollTop = this.state.y;

  }

  handleSwitchScrollControlMoving = () => {

    this.setState({
      enableMoveScrollControl: true
    })

  }

  render() {

    const { startScrolling, y, maxY } = this.state;

    return(
      <div className={`scroll-line-wrap ${startScrolling ? '__moving-theme' : ''}`} id="scroll-control">
        <div className="scroll-slider">
          <Slider
            axis="y"
            y={y}
            ymax={maxY}
            onChange={this.handleChangeSlider}
            onDragEnd={this.handleSwitchScrollControlMoving}
            className="custom-slider"
          />
        </div>
        <div className="scroll-line-wrap-title" id="scroll-control-label">Scroll</div>
      </div>
    )

  }

}

export default ScrollControl
