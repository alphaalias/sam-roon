import React, { Component } from 'react'
import Link from 'gatsby-plugin-transition-link'

let timeLeftContainer;
let cookieMessageContainer;

class CookieContainer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      cookieMessage: true 
    }
  }

  componentDidMount() {

    setTimeout(function() {

      this.setState({
        cookieMessage: Boolean(JSON.parse(localStorage.getItem('hideCookieMessage')))
      });

      timeLeftContainer = document.getElementById('time-left-wrap');
      cookieMessageContainer = document.getElementById('cookie-wrap');

      if(!this.state.cookieMessage && window.innerWidth > 768 && timeLeftContainer) {

        timeLeftContainer.style.bottom = (cookieMessageContainer.clientHeight - timeLeftContainer.clientHeight) + 'px'

      }

    }.bind(this), 2000);

  }

  handleHideCookieMessage = () => {

    localStorage.setItem('hideCookieMessage', true);
    this.setState({
      cookieMessage: true
    })

    timeLeftContainer.style.bottom = '-102px'

  }

  render() {

    const { cookieMessage } = this.state;

    return(

      <div className={`cookie-message-wrap ${ cookieMessage ? '' : 'is-visible' }`} id="cookie-wrap">
        <div className="container">
          <div className="subtitle">
            <p>This website uses cookies</p>
          </div>
          <div className="cookie-message-text">
            <p>We use cookies to analyze website traffic. We use this information to optimize our website and interactions with our website visitors. We use 3rd party analytics tools, like Google Analytics, but we never share data, personal or anonymized, with third parties for commercial purposes. By continuing to use this website you accept the use of cookies.</p>
          </div>
          <Link
            exit={{
              length: 1
            }}
            entry={{
              delay: 0
            }}
            to="/privacy/"
            className="default-link">View Our Privacy Policy</Link>
        </div>
        <button className="close-message" onClick={this.handleHideCookieMessage}></button>
      </div>

    )

  }

}

export default CookieContainer