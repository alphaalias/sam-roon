import React, { Component } from 'react'
import { graphql } from 'gatsby'

class FooterAddress extends Component {

  render() {

    const data = this.props.data;

    return(
      
        
        <div className="footer-content-panel-item address">
          {/* <p className="footer-nav-title">Visit Us</p> */}
          <div dangerouslySetInnerHTML={{ __html: data.allWordpressAcfOptions.edges[0].node.options.address }} className="footer-nav-link" />
        </div>
        
      
    )

  }

}

export default FooterAddress

