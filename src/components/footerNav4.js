import React, { Component } from 'react'
import FooterNav from "./footerNav"
import { graphql, StaticQuery } from 'gatsby'

class FooterNav4 extends Component {

  render() {

    const data = this.props.data;

    return(
      
        
        <div className="footer-content-panel-item">
          
          <StaticQuery
            query={GET_FOOTER_MENU4}
            render={data => <FooterNav data={data} />}
          />
        </div>
        
      
    )

  }

}

export default FooterNav4

const GET_FOOTER_MENU4 = graphql`
    {
        wordpressWpApiMenusMenusItems (id: { eq: "77640c6e-a62b-58cd-ba81-37d11fd9b197" }){
            items{
                title,
                url
            }
        }
    }
`