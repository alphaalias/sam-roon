import React, { Component } from 'react'
import FooterNav from "./footerNav"
import { graphql, StaticQuery } from 'gatsby'

class FooterNav3 extends Component {

  render() {

    const data = this.props.data;

    return(
      
        
        <div className="footer-content-panel-item">
          
          <StaticQuery
            query={GET_FOOTER_MENU3}
            render={data => <FooterNav data={data} />}
          />
        </div>
        
      
    )

  }

}

export default FooterNav3

const GET_FOOTER_MENU3 = graphql`
    {
        wordpressWpApiMenusMenusItems (id: { eq: "a4b495db-518a-5479-8d60-5b3d351a11db" }){
            items{
                title,
                url
            }
        }
    }
`