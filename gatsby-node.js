const path = require(`path`)
const slash = require(`slash`)
const createPaginatedPages = require('gatsby-paginate')

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions

  const result = await graphql(`
    {
      allWordpressPage {
        edges {
          node {
            id
            slug
            status
            template
          }
        }
      }
      allWordpressPost {
        edges {
          node {
            id
            path
            status
            template
            format
          }
        }
      }
      allWordpressWpCasesPosts{
        edges{
          node{
            id,
            title,
            slug,
            acf{
              logo{
                localFile{
                  childImageSharp{
                    fluid (maxWidth: 122){
                      aspectRatio,
                      base64,
                      sizes,
                      src,
                      srcSet
                    }
                  }
                }
              }
            }
            featured_media{
              localFile{
                childImageSharp{
                  fluid (maxWidth: 1000){
                    aspectRatio,
                    base64,
                    sizes,
                    src,
                    srcSet
                  }
                }
              }
            }
          }
        }
      }
      allWordpressWpCareersPosts{
        edges{
          node{
            id,
            slug,
            date (formatString: "DD.MM.YY"),
            acf{
              main_title,
              slogan,
              city,
              folder_description,
              folder_full_description,
              due_date_title,
              due_date
            }
          }
        }
      }
      allWordpressWpNewsPosts{
        edges{
          node{
            id,
            slug,
            title,
            categories{
              id,
              name
            }
          }
        }
      }
      allWordpressCategory{
        edges{
          node{
            id,
            slug
          }
        }
      }
      allWordpressTag{
        edges{
          node{
            id,
            slug
          }
        }
      }
      allWordpressWpPillar{
        edges{
          node{
            id,
            slug
          }
        }
      }
    }
  `)

  // const result = await graphql(`
  //   {
  //     allWordpressPost {
  //       edges {
  //         node {
  //           id
  //           path
  //           status
  //           template
  //           format
  //         }
  //       }
  //     }
  //   }
  // `)

  if (result.errors) {
    throw new Error(result.errors)
  }

  const {
    allWordpressPage,
    allWordpressWpCasesPosts,
    allWordpressWpCareersPosts,
    allWordpressWpNewsPosts,
    allWordpressCategory,
    allWordpressTag,
    allWordpressWpPillar
  } = result.data

  //Create Page pages.
  const pageTemplate = path.resolve(`./src/templates/pageTemplate.js`)
  allWordpressPage.edges.forEach(edge => {
    createPage({
      path: edge.node.slug,
      component: slash(pageTemplate),
      context: {
        id: edge.node.id,
      },
    })
  })

  // const postTemplate = path.resolve(`./src/templates/post.js`)
  // allWordpressPost.edges.forEach(edge => {
  //   createPage({
  //     path: edge.node.path,
  //     component: slash(postTemplate),
  //     context: {
  //       id: edge.node.id,
  //     },
  //   })
  // })

  createPaginatedPages({
    edges: allWordpressWpCasesPosts.edges,
    createPage: createPage,
    pageTemplate: 'src/templates/cases.js',
    pageLength: 4,
    pathPrefix: 'cases',
  })

  const casesPostTemplate = path.resolve(`./src/templates/case.js`)
  allWordpressWpCasesPosts.edges.forEach(edge => {
    createPage({
      path: '/cases/' + edge.node.slug,
      component: slash(casesPostTemplate),
      context: {
        id: edge.node.id,
      },
    })
  })

  if (allWordpressWpCareersPosts) {

    createPaginatedPages({
      edges: allWordpressWpCareersPosts.edges,
      createPage: createPage,
      pageTemplate: 'src/templates/careers.js',
      pageLength: 4,
      pathPrefix: 'careers',
    })

    const careersPostTemplate = path.resolve(`./src/templates/jobSingle.js`)
    allWordpressWpCareersPosts.edges.forEach(edge => {
      createPage({
        path: '/careers/' + edge.node.slug,
        component: slash(careersPostTemplate),
        context: {
          id: edge.node.id,
        },
      })
    })

  }

  const newPostTemplate = path.resolve(`./src/templates/newSingle.js`)
  allWordpressWpNewsPosts.edges.forEach(edge => {
    if (edge.node.categories) {
      createPage({
        path: '/blog/' + (edge.node.categories[0].name).toLowerCase() + '/' + edge.node.slug,
        component: slash(newPostTemplate),
        context: {
          id: edge.node.id,
        },
      })
    } else {
      createPage({
        path: '/blog/' + edge.node.slug,
        component: slash(newPostTemplate),
        context: {
          id: edge.node.id,
        },
      })
    }
  })

  const categoryTemplate = path.resolve(`./src/templates/category.js`)
  allWordpressCategory.edges.forEach(edge => {
    if (edge) {
      createPage({
        path: '/blog/' + edge.node.slug,
        component: slash(categoryTemplate),
        context: {
          id: edge.node.id
        },
      })
    }
  })

  const tagTemplate = path.resolve(`./src/templates/archive.js`)
  allWordpressTag.edges.forEach(edge => {
    createPage({
      path: '/blog/tags/' + edge.node.slug,
      component: slash(tagTemplate),
      context: {
        id: edge.node.id
      },
    })
  })

  const pillarTemplate = path.resolve(`./src/templates/pillarSingle.js`)
  allWordpressWpPillar.edges.forEach(edge => {
    createPage({
      
      path: '/' + edge.node.slug,
      component: slash(pillarTemplate),
      context: {
        id: edge.node.id
      },
    })
  })

}